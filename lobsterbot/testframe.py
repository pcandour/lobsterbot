import sys, imp, traceback
import plugins.libs.sqllib as sqllib
import plugins.libs.cachelib as cachelib

class irc():
  owner = 'Owner'
  def msg(self, channel, msg):
    print ('%s: %s' % (channel, msg))
  def describe(self, channel, msg):
    print ('%s %s' % (channel, msg))
  def trimPrefix(self, line, prefix=''):
    return line.replace('%s ' % prefix, '', 1)
  def firstArg(self, line):
    return line.split(' ')[0]
  def splitArgs(self, args, prefix=None):
    if prefix:
      args = self.trimPrefix(args)
    a = args.partition(' ')
    return (a[0], a[2])


class checkModule:
  command_dir = '/var/cache/plugins'
  modules = []

  def __init__(self):
    self.db = sqllib.sqllib()
    self.db.connect('/var/cache/remember.sql')
    self.cache = cachelib.Cache()
    self.useful = {'db': self.db, 'cache': self.cache}

  def checkModule(self, moduleName = ''):
    print ('Loading module %s' % moduleName)
    if moduleName in sys.modules:
      del sys.modules[moduleName]
    fp, pathname, desc = imp.find_module(moduleName, [self.command_dir])
    imp.load_module(moduleName, fp, pathname, desc)

  def run(self, moduleName, channel='test', user='Owner', args = ''):
    self.checkModule(moduleName)
    ircmock = irc()
    try:
      return sys.modules[moduleName].run(ircmock, channel, user, args, self.useful)
    except Exception as e:
      print ("Error in %s:" % moduleName)
      print (traceback.format_exc(4))
      return "Error executing module"

cm = checkModule()
cm.run('youtube', args='summer')
cm.run('convert', args='10 cm to inches')
cm.run('runevery-hidden', args='imgur http://imgur.com/gallery/xesB5')
cm.run('weather', args='london')
