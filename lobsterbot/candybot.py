#!/usr/bin/python3

from twisted.words.protocols import irc
from twisted.internet import ssl, reactor, protocol, threads
from twisted.python import log
from datetime import datetime

import time, sys, imp, os, re
import plugins.libs.cachelib as cachelib
import plugins.libs.sqllib as sqllib
import traceback


class checkModule:
  moduletimes = {}
  modules = []
  command_dir = '/var/cache/plugins'

  def __init__(self, logger):
    self.logger = logger

  def checkModule(self, moduleName = ''):
    if moduleName == '' or moduleName not in self.modules:
      return

    stat = os.stat('%s/%s.py' % (self.command_dir, moduleName))
    time = stat[8]

    try:
      if time > self.moduletimes[moduleName]:
        self.logger.log('Reloading module %s' % moduleName)
        del sys.modules[moduleName]
        fp, pathname, desc = imp.find_module(moduleName, [self.command_dir])
        imp.load_module(moduleName, fp, pathname, desc)
        self.moduletimes[moduleName] = time
    except:
      self.moduletimes[moduleName] = time

  def isModule(self, moduleName):
    if moduleName in self.modules:
      return 1
    self.logger.log("Trying to load module %s" % moduleName)
    try:
      fp, pathname, desc = imp.find_module(moduleName, [self.command_dir])
    except:
      self.logger.log("Could not find %s" % moduleName)
      return 0
    try:
      imp.load_module(moduleName, fp, pathname, desc)
    except Exception as e:
      self.logger.log("Could not load %s because %s" % (moduleName, e))
      return 0

    self.modules.append(moduleName)
    self.logger.log("Loaded module %s" % moduleName)
    return 1

  def unloadLibrary(self, moduleName):
    imp.reload(sys.modules[moduleName])

  def unloadModule(self, moduleName):
    self.logger.log("Unloading module %s" % moduleName)
    if moduleName in self.modules:
      self.modules.remove(moduleName)
      return 1
    return 0

  def listModules(self):
    reply = []
    dirlist = os.listdir(self.command_dir)
    for entry in dirlist:
      if entry != '__init__.py' and entry.endswith('.py') and not entry.endswith('-hidden.py'):
        entry = entry[0:len(entry)-3]
        reply.append(entry)
    return reply

  def strListModules(self):
    return ', '.join(self.listModules())

  def trueListModules(self):
    reply = []
    dirlist = os.listdir(self.command_dir)
    for entry in dirlist:
      if entry != '__init__.py' and entry.endswith('.py'):
        entry = entry[0:len(entry)-3]
        reply.append(entry)
    return reply

  def unloadAllModules(self):
    for mod in self.modules:
      self.unloadModule(mod)

  def moduleInfo(self, moduleName):
    if moduleName in self.moduletimes:
      return self.moduletimes[moduleName]
    return 0

  def run(self, irc, moduleName, channel, user, useful, args = ''):
    if self.isModule(moduleName):
      self.checkModule(moduleName)
      try:
        if moduleName in ['exchange', 'convert']:
          resp = sys.modules[moduleName].run(irc, channel, user, args, useful)
          #return resp
        else:
          threads.deferToThread(sys.modules[moduleName].run, irc, channel, user, args, useful)
      except Exception as e:
        self.logger.log("Error in %s:" % moduleName)
        self.logger.log(traceback.format_exc(4))
        return "Error executing module"
    else:
      return 'Command not found'

  def help(self, moduleName):
    if self.isModule(moduleName):
      self.checkModule(moduleName)
      try:
        return sys.modules[moduleName].help()
      except Exception as e:
        self.logger.log("Error in %s:" % moduleName)
        self.logger.log(e)
        self.logger.log('End of error')
        return "Error executing module"
    else:
      return 'Command not found'

  def owner(self, moduleName):
    if self.isModule(moduleName):
      self.checkModule(moduleName)
      if 'owner' in dir(sys.modules[moduleName]):
        return sys.modules[moduleName].owner()
      else:
        return 0
    else:
      return 'Command not found'


class MessageLogger:
  """
  An independent logger class (because separation of application
  and protocol logic is a good thing).
  """
  def __init__(self, file):
    self.file = file

  def log(self, message):
    """Write a message to the file."""
    timestamp = time.strftime("[%d%m%Y %H:%M:%S]", time.localtime(time.time()))
    self.file.write('%s %s\n' % (timestamp, message))
    self.file.flush()

  def close(self):
    self.file.close()


class LogBot(irc.IRCClient):
  """A logging IRC bot."""

  # List at: http://twistedmatrix.com/documents/12.0.0/api/twisted.words.protocols.irc.IRCClient.html

  def __init__(self):
    self.nickname = 'LobsterBot'
    self.owner = ''
    self.channels = []
    self.lastspoke = int(time.time())

    self.db = sqllib.sqllib()
    self.db.connect('/var/cache/remember.sql')
    self.cache = cachelib.Cache()
    self.useful = {'db': self.db, 'cache': self.cache}

  # Some convenience methods:
  def trimPrefix(self, line, prefix=''):
    return line.replace('%s ' % prefix, '', 1)

  def firstArg(self, line):
    return line.split(' ')[0]

  def splitArgs(self, args, prefix=None):
    if prefix:
      args = self.trimPrefix(args)
    a = args.partition(' ')
    return (a[0], a[2])

  def connectionMade(self):
    irc.IRCClient.connectionMade(self)
    self.logger = MessageLogger(open(self.factory.filename, "a"))
    self.logger.log("[connected at %s]" %
            time.asctime(time.localtime(time.time())))
    self.cM = checkModule(self.logger)

  def connectionLost(self, reason):
    irc.IRCClient.connectionLost(self, reason)
    self.logger.log("[disconnected at %s]" %
            time.asctime(time.localtime(time.time())))
    self.logger.close()

  # callbacks for events
  def nickChanged(self, nick):
    """Called when my nick changes."""
    self.setNick(self.nickname)

  def signedOn(self):
    """Called when bot has succesfully signed on to server."""
    self.join(self.factory.channel)
    self.join('#lobsterbot')

  def joined(self, channel):
    """This will get called when the bot joins the channel."""
    self.channels.append(channel)
    self.logger.log("[I have joined %s]" % channel)

  def left(self, channel):
    """This will get called when the bot leaves a channel."""
    self.channels.remove(channel)
    self.logger.log("[I have left %s]" % channel)

  def userQuit(self, user, quitMessage):
    if user == self.owner:
      self.owner = None

  def userLeft(self, user, channel):
    self.userQuit(user, 'User %s left channel #%s' % (user, channel))
    if user == self.owner:
      self.owner = None

  def userKicked(self, kickee, channel, kicker, message):
    self.userQuit(kickee, 'User %s kicked by %s from channel #%s for "%s"' % (kickee, kicker, channel, message))
    if kickee == self.owner:
      self.owner = None

  def action(self, user, channel, data):
    return self.privmsg(user, channel, data)

  def privmsg(self, user, channel, msg):
    """This will get called when the bot receives a message."""
    self.lastspoke = int(time.time())
    user = user.split('!', 1)[0]
    if user == 'forgeirc' and '>' in msg:
      user = msg[1:msg.index('>')]
      msg = msg[msg.index('>')+2:]

    # Check to see if they're sending me a private message
    resp = None
    if channel.lower() == self.nickname.lower():
      resp = user
      channel = user
    elif msg.startswith(self.nickname + ":"):
      resp = channel
      msg = msg[msg.find(': ')+2:].strip()

    if not resp:
      if user in ['BOTS']:
        return

      if msg.lower().startswith('!'):
        if ' ' in msg:
          cmd, args = msg.lower().split(' ', 1)
        else:
          cmd = msg
          args = ''
        db = sqllib.sqllib()
        db.connect('/var/cache/remember.sql')
        db.executeArgs('SELECT plugin FROM aliases WHERE alias=?', (cmd, ))
        res = db.fetchOne()
        if res:
          self.cM.run(self, res[0], channel, user, self.useful, args)
          return

      # Not directly for me, but might contain a link
      else:
        resp = self.cM.run(self, 'runevery-hidden', channel, user, self.useful, msg)
      return

    if msg == 'identify PASSWORD':
      self.owner = user
      self.msg(resp, 'Owned')
      self.logger.log('Now owned by %s' % user)
      return

    if ' ' in msg:
      (cmd, args) = msg.split(' ', 1)
      cmd = cmd.lower()
    else:
      cmd = msg.lower()
      args = ''

    if user == self.owner:
      if cmd == 'reload' and args != '':
        args = self.firstArg(args)
        if args in self.cM.trueListModules():
          self.msg(user, 'Reloading %s' % args)
          self.cM.checkModule(args)
        return

      if cmd == 'unload' and args != '':
        args = self.firstArg(args)
        if self.cM.unloadModule(args):
          self.msg(user, 'Unloaded module.')
        else:
          self.msg(user, 'Failed to unload module.')
        return

      if cmd == 'unloadall' and args != '':
        self.cM.unloadAllModules()
        return

      if cmd == 'unloadlib' and args != '':
        args = self.firstArg(args)
        self.cM.unloadLibrary(args)
        return

      if cmd == 'diediedie':
        self.msg(user, 'Dying :(')
        self.quit('Suicide time!')
        reactor.stop()
        sys.exit()
        return

      if cmd == 'moduleinfo':
        module = self.firstArg(args)
        self.msg(user, 'Getting info for %s' % module)
        if module and module in self.cM.trueListModules():
          self.msg(user, 'Update time: %s' % time.ctime(self.cM.moduleInfo(module)))
        if module in sys.modules and 'version' in dir(sys.modules[module]):
          self.msg(user, 'Version: %d' % sys.modules[module].version)
        return

    if cmd == 'help':
      if args == '':
        self.msg(resp, 'I can: %s' % self.cM.strListModules())
        return

      if ' ' in args:
        args = args[0:args.find(' ')]

      if args in self.cM.listModules():
        self.msg(resp, self.cM.help(args))
      else:
        self.msg(resp, 'Unknown command %s.' % args)
      return

    if cmd in self.cM.listModules():
      self.logger.log('Running regular %s for %s' % (cmd, user))
      if self.owner != user:
        if self.cM.owner(cmd):
          self.msg(resp, 'Sorry, you don\'t have permission to do that.')
        else:
          resp = self.cM.run(self, cmd, channel, user, self.useful, args)
      else:
        resp = self.cM.run(self, cmd, channel, user, self.useful, args)
      if resp:
        self.logger.log('Output from cmd: %s' % resp)
      return

    if cmd in self.cM.trueListModules() and self.owner == user:
      self.logger.log('Running privileged %s for %s' % (cmd, user))
      resp = self.cM.run(self, cmd, channel, user, self.useful, args)
      if resp:
        self.logger.log('Output from cmd: %s' % resp)

    db = sqllib.sqllib()
    db.connect('/var/cache/remember.sql')
    db.executeArgs('SELECT plugin FROM aliases WHERE alias=?', (cmd, ))
    res = db.fetchOne()
    if res:
      self.cM.run(self, res[0], channel, user, self.useful, args)
      return

  # irc callbacks

  def irc_NICK(self, prefix, params):
    """Called when an IRC user changes their nickname."""
    old_nick = prefix.split('!')[0]
    new_nick = params[0]
    self.logger.log("%s is now known as %s" % (old_nick, new_nick))
    if old_nick == self.owner:
      self.owner = ''


  # For fun, override the method that determines how a nickname is changed on
  # collisions. The default method appends an underscore.
  def alterCollidedNick(self, nickname):
    """
    Generate an altered version of a nickname that caused a collision in an
    effort to create an unused related name for subsequent registration.
    """
    return nickname + '^'


class LogBotFactory(protocol.ClientFactory):
  """A factory for LogBots.

  A new protocol instance will be created each time we connect to the server.
  """
  def __init__(self, channel, filename):
    self.channel = channel
    self.filename = filename

  def buildProtocol(self, addr):
    p = LogBot()
    p.factory = self
    return p

  def clientConnectionLost(self, connector, reason):
    """If we get disconnected, reconnect to server."""
    connector.connect()

  def clientConnectionFailed(self, connector, reason):
    print ("connection failed:", reason)
    reactor.stop()


if __name__ == '__main__':
  # initialize logging
  log.startLogging(sys.stdout)
  # create factory protocol and application
  f = LogBotFactory('#lobsterbot', 'output.log')
  # connect factory to this host and port
  reactor.connectSSL("irc.server.name", 6697, f, ssl.ClientContextFactory())
  # run bot
  reactor.run()
