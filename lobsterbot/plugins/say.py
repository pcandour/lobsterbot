def run(irc, channel, user, args, useful):
  if args == '' or ' ' not in args or user != irc.owner:
    irc.msg(user, 'What?')
    return

  channel, args = args.split(' ', 1)

  irc.msg(channel, args)

def help():
  return 'Usage: say <msg>.'
