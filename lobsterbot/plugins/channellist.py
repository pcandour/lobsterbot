def run(irc, channel, user, args, useful):
  irc.msg(user, 'I am connected to: %s' % ', '.join(irc.channels))

def owner():
  return 1

def help():
  return 'Usage: channellist. Lists the channels I am connected to.'
