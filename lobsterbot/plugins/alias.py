import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, '%s: What do you want me to alias?' % user)
    return

  a = args.split(' ')

  alias = a[0]

  if not alias.isalpha() and not (alias.startswith('!') and alias[1:].isalpha()):
    irc.msg(channel, '%s: Invalid alias or target.' % user)
    return

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')
  db.executeArgs('SELECT plugin FROM aliases WHERE alias=?', (alias, ))
  res = db.fetchOne()
  if res:
    irc.msg(channel, '%s: %s is aliased to %s.' % (user, alias, res[0]))
    return

  if len(a) < 2:
    irc.msg(channel, '%s: What do you want me to alias?' % user)
    return

  target = a[1]
  if not target.isalpha():
    irc.msg(channel, '%s: Invalid alias or target.' % user)
    return

  db.execArgsAndCommit('INSERT INTO aliases (alias, plugin) VALUES (?, ?)', (alias, target))
  irc.msg(channel, "%s: Aliasing %s to %s." % (alias, target))

def help():
  return 'Usage: alias <alias> <target>'

def owner():
  return True
