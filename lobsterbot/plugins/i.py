import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  if args == '':
    db.executeArgs('SELECT location, name FROM userinfo WHERE user=?', (user, ))
    res = db.fetchOne()
    if not res:
      irc.msg(channel, '%s: I don\'t have any information about you.')
      return

    if res[0]:
      if res[1]:
        irc.msg(channel, '%s: Your name is %s and you live in %s.' % (user, str(res[1]), str(res[0])))
      else:
        irc.msg(channel, '%s: You live in %s' % (user, str(res[0])))
    else:
      irc.msg(channel, '%s: Your name is %s' % (user, str(res[1])))
    return

  if args.lower().startswith('live in'):
    args = args[8:]

    db.executeArgs('INSERT OR IGNORE INTO userinfo (user, location) VALUES (?, ?)', (user, str(args)))
    db.execArgsAndCommit('UPDATE userinfo SET location=? WHERE user=?', (str(args), user))
    irc.msg(channel, "%s: Okay I'll remember you live in %s." % (user, str(args)))

  if args.lower().startswith('am called'):
    args = args[10:]

    db.executeArgs('INSERT OR IGNORE INTO userinfo (user, name) VALUES (?, ?)', (user, str(args)))
    db.execArgsAndCommit('UPDATE userinfo SET name=? WHERE user=?', (str(args), user))
    irc.msg(channel, "%s: Okay I'll remember you are called %s." % (user, str(args)))

  if args.lower().startswith('was born on'):
    from dateutil import parser
    args = args[12:]
    date = parser.parse(args).strftime('%d-%m-%Y')

    db.executeArgs('INSERT OR IGNORE INTO userinfo (user, birthday) VALUES (?, ?)', (user, str(args)))
    db.execArgsAndCommit('UPDATE userinfo SET birthday=? WHERE user=?', (str(args), user))
    irc.msg(channel, "%s: Okay I'll remember you were born %s." % (user, str(date)))

def help():
  return 'Usage: I live in/am called/was born on <something>.'
