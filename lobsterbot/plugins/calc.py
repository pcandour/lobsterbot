import math, re
integers_regex = re.compile(r'\b[\d\.]+\b')

def calc(expr, advanced=False):

  def safe_eval(expr, symbols={}):
    return eval(expr, dict(__builtins__=None), symbols)

  def whole_number_to_float(match):
    group = match.group()
    if group.find('.') == -1:
      return group + '.0'
    return group

  expr = expr.replace('^','**')
  expr = integers_regex.sub(whole_number_to_float, expr)
  if advanced:
    return safe_eval(expr, vars(math))
  else:
    try:
      return safe_eval(expr)
    except Exception as e:
      return 'an invalid request.'


def run(irc, channel, user, args, useful):
  if args.startswith('!calc'):
    args = args[5:]

  cargs = args.replace('pie', str(math.pi))
  cargs = args.replace('pi', str(math.pi))

  irc.msg(channel, '%s: %s is %s' % (user, args, str(calc(cargs))))

def help():
  return 'Usage: Calc <expression>.'
