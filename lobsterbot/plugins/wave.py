def run(irc, channel, user, args, useful):
  if args == '':
    irc.describe(channel, 'waves at %s.' % user)
    return

  if ' ' in args:
    argsplit = args.split(' ')
    if len(argsplit) > 1 and argsplit[0].lower() == 'at':
      argsplit.remove('at')
      args = ' '.join(argsplit)

  irc.describe(channel, 'waves at %s.' % args)

def help():
  return 'Usage: wave <user>. Waves at somebody.'
