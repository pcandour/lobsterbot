def run(irc, channel, user, args, useful):
  import random

  intChance = random.randint(0, 100)
  if (intChance == 0 and random.randint(0, 3) == 0):
    chance = "one in a million"
  else:
    chance = str(intChance) + "%"

  if args:
    if args.startswith('!chance'):
      args = args[8:]

    if args.lower().startswith('i '):
      args = 'you %s' % args[2:]
    if args.lower().startswith('my '):
      args = 'your %s' % args[3:]
    args = args.replace(' i ', ' you ').replace(' I ', ' you ').replace(' my ', ' your ')
    
    if args == 'ding' or args == 'tea':
      if user.lower() == 'ng':
        irc.msg(channel, "%s: There is a 0%% chance %s." % (user, args))
      else:
        irc.msg(channel, "%s: There is a 100%% chance %s but not for Ng. He gets dong instead." % (user, args))
    else:
      irc.msg(channel, "%s: There is a %s chance %s." % (user, chance, args))
  else:
    irc.msg(channel, "%s: There is a %s chance." % (user, chance))

def help():
  return 'Usage: Chance <thing>.'
