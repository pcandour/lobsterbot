def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, help())
    return

  chan = irc.firstArg(args)
  irc.msg(user, 'Okay, leaving %s.' % chan)
  irc.leave(chan)

def owner():
  return 1

def help():
  return 'Usage: channelleave <channel>.'
