def run(irc, channel, user, args, useful):

  import plugins.libs.soundcloudlib as soundcloudlib
  c = soundcloudlib.SoundCloud()

  if args.startswith('http'):
    url = args.split(' ')[0]
    track = c.geturl(url)
    if 'error' in track:
      irc.msg(channel, '%s: Soundcloud error: %s.' % (user, track['error']))
    irc.msg(channel, '%s: Soundcloud results: \'%s\' by \'%s\'' % (user, track['title'], track['user']))
    return 'soundcloud http'

  track = c.search(args)
  if 'error' in track:
    irc.msg(channel, '%s: Soundcloud error: %s.' % (user, track['error']))

  irc.msg(channel, '%s: Soundcloud results: \'%s\' by \'%s\': %s' % (user, track['title'], track['user'], track['url']))

def help():
  return 'soundcloud <search string>. Does a search on soundcloud and returns the first result.'
