import urllib2

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(user, 'Which site?')
    return

  if not args.startswith('http://') and not args.startswith('https://'):
    args = 'https://' + args

  res = None
  try:
    res = urllib2.urlopen(args)
  except urllib2.URLError as e:
    irc.msg(channel, '%s: Seems down: %s' % (user, e))
  except Exception as e:
    irc.msg(channel, '%s: Something broken: %s' % (user, e))

  if res:
    irc.msg(channel, '%s: Looks fine to me.' % user)

def help():
  return 'Usage: isup <site>.'
