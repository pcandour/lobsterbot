import HTMLParser
import time
import re
import plugins.libs.bbcnewslib as bbcnewslib
import plugins.libs.youtubelib as youtubelib
import plugins.libs.vimeolib as vimeolib
import plugins.libs.cachelib as cachelib
import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):

  if user in ['BOTNAMES']:
    return

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')
  c = cachelib.Cache()

  cc = len(args) + 1
  argsp = args.split(' ')
  wc = len(argsp)
  cc = cc - wc

  db.executeArgs('INSERT OR IGNORE INTO wordstats VALUES (?, 0, 0, 0, date(\'now\'))', (user.lower(), ))
  db.execArgsAndCommit('UPDATE wordstats SET numwords = numwords + ?, charcount = charcount + ? WHERE user = ?', (wc, cc, user.lower()))

  graph = None
  for word in ['pork', 'boobs', 'peen']:
    if word in args.lower():
      if not graph:
        graph = sqllib.sqllib()
        graph.connect('/var/cache/graph.sql')
      graph.execArgsAndCommit('INSERT INTO graphdata (user, key, datetime, value) VALUES (?, ?, strftime(\'%s\', "now"), 1)', (user, word))

  if 'http' in args.lower():
    parser = HTMLParser.HTMLParser()

    ytids = []

    if 'youtube.com/' in args.lower() or '//youtu.be/' in args.lower():
      ytids = re.findall('https?:\/\/www.youtube.com\/[^ ]*v=([-_A-Za-z0-9]{11})', args)
      if not ytids:
        ytids = re.findall('https?:\/\/youtu.be\/[^ ]*([-_A-Za-z0-9]{11})', args)

      if ytids:
        titles = list()

        for ytid in ytids:
          entry = youtubelib.youtube_get_video(ytid)
          try:
            titles.append(u'%s (%s)' % (entry[0], entry[1]))
          except Exception as e:
            titles.append('Failed decode: %s. (%s)' % (e, entry[1]))

        if titles:
          irc.msg(channel, '%s: Youtube results - "%s"' % (user, '", "'.join(titles)))

    if 'vimeo.com' in args.lower():
      v = vimeolib.VimeoLib()
      keys = re.findall('https?:\/\/vimeo.com\/([0-9]{8})', args)
      titles = list()
      for key in keys:
        title = c.getFromCache('vimeo-%s' % key, timeout=8640000)

        if not title:
          try:
            title = v.Lookup(key)
          except VimeoError:
            continue

          c.updateCache('vimeo-%s' % key, title)
        titles.append('"%s"' % title)

      if titles:
        irc.msg(channel, '%s: Vimeo details: %s.' % (user, ' '.join(titles)))

    lowargs = args.lower()
    if 'bbc.co.uk/news' in lowargs or 'imgur.com/gallery' in lowargs or 'xkcd.com/' in lowargs or 'slashdot.org' in lowargs or 'theregister.co.uk' in lowargs or 'bbc.com/news' in lowargs or 'twitter.com/' in lowargs:
      v = bbcnewslib.BbcNews()
      urls = set()
      urls.update(re.findall('https?:\/\/www\.bbc\.co\.uk\/news\/[a-z0-9-]+-[0-9]{8}', args))
      urls.update(re.findall('https?:\/\/www\.bbc\.com\/news\/[a-z0-9-]+-[0-9]{8}', args))
      urls.update(re.findall('https?:\/\/imgur\.com\/gallery\/[a-zA-Z0-9-]+', args))
      urls.update(re.findall('https?:\/\/xkcd\.com\/[0-9-]+', args))
      urls.update(re.findall('https?:\/\/[a-z]+\.slashdot\.org\/story\/[a-zA-Z0-9-_/]+', args))
      urls.update(re.findall('https?:\/\/www\.theregister\.co\.uk\/[a-zA-Z0-9-_/]+', args))
      urls.update(re.findall('https:\/\/twitter\.com\/[^ ]+', args))
      titles = list()
      for url in urls:
        print(url)
        title = v.CachedTitleLookup(url)
        if title:
          titles.append(title)
      if titles:
        irc.msg(channel, '%s: %s' % (user, ' '.join(titles)))


def owner():
  return True

def help():
  return 'Usage: None.'
