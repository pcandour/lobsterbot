import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, '%s: Where do you live?' % user)
    return

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  db.execArgsAndCommit('DELETE FROM location WHERE user=?', (user, ))
  db.execArgsAndCommit('INSERT INTO location VALUES (?, ?)', (user, str(args)))
  irc.msg(channel, "%s: Okay I'll remember you live in %s" % (user, str(args)))

def help():
  return 'Usage: ilivein <somewhere>.'
