def run(irc, channel, user, args, useful):
  if args == '':
    irc.describe(channel, 'sits on %s.' % user)
    return

  if ' ' in args:
    argsplit = args.split(' ')
    if len(argsplit) > 1 and argsplit[0].lower() == 'on':
      argsplit.remove('on')
      args = ' '.join(argsplit)

  irc.describe(channel, 'sits on %s.' % args)

def help():
  return 'Usage: sit <user>. Sits on somebody.'
