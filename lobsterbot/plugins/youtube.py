import plugins.libs.youtubelib as youtubelib

def run(irc, channel, user, args, useful):
  if args == '':
    return

  title, id, length = youtubelib.youtube_search(args)['videos'][0]
  irc.msg(channel, '%s: "%s" (%ss) -  https://www.youtube.com/watch?v=%s' % (user, title, length, id))

def help():
  return 'youtube <search string>. Does a search on youtube and returns the first result.'
