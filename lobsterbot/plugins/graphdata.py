import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, '%s: What do you want know from the graph?' % user)
    return

  a = args.split(' ')
  key = a[0]
  if len(a) > 2:
    keyuser = a[2]
  else:
    keyuser = None
  keyuser = None
  key = args

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  if keyuser:
    db.executeArgs('SELECT SUM(value) FROM graphdata WHERE key=? AND user=?', (key, keyuser))
  else:
    db.executeArgs('SELECT SUM(value) FROM graphdata WHERE key=?', [key])
  res = db.fetchAll()
  if len(res) == 0:
    irc.msg(channel, "%s: There are no %s." % (user, args))
  else:
    value = res[0][0]
    if keyuser:
      irc.msg(channel, "%s: There is a total of %s %s graphed by %s." % (user, value, key, keyuser))
    else:
      irc.msg(channel, "%s: There is a total of %s %s." % (user, value, key))

def help():
  return 'Usage: graphdata <key>[ for <user>].'
