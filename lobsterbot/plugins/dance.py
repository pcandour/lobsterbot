def run(irc, channel, user, args, useful):
  if user == 'tahn':
    irc.msg(channel, 'tahn: I will never dance with you.')
    return

  if args == '':
    irc.describe(channel, 'dances with %s.' % user)
    return 'solo'

  if ' ' in args:
    argsplit = args.split(' ')
    if len(argsplit) > 1 and argsplit[0].lower() == 'with':
      argsplit.remove('with')
      args = ' '.join(argsplit)

  if 'tahn' in args:
    irc.msg(channel, 'tahn: I will never dance with you.')
    return

  irc.describe(channel, 'dances with %s.' % args)

def help():
  return 'Usage: dance <user>. Dances with somebody.'
