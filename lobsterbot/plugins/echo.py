def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, 'You are "%s" and you said nothing.' % user)
    return

  irc.msg(channel, 'You are "%s" and you said "%s".' % (user, args))

def help():
  return 'Usage: echo <something>.'
