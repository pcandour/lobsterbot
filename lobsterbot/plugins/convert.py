from sympy.physics import units
import math
import re
import plugins.exchange as exchange

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, help())
    return

  maths = {'pi': math.pi}

  units.metres = units.meters
  units.metre = units.meters
  units.lightseconds = 299792458 * units.meters
  units.ls = units.lightseconds
  units.week = 7 * units.days
  units.weeks = 7 * units.days
  units.stones = 6.35029318 * units.kg
  units.stone = 6.35029318 * units.kg
  units.kcals = 4.184 * units.joules
  units.cals = 4.184 * units.joules
  units.calories = 4.184 * units.joules
  units.lb = 0.453592 * units.kg
  units.lbs = 0.453592 * units.kg
  units.pounds = 0.453592 * units.kg
  units.oz = 0.0283495 * units.kg
  units.ounces = units.oz
  units.kgs = units.kg
  units.americangallons = 3.78541 * units.liters
  units.usgallons = 3.78541 * units.liters
  units.gallons = 4.54609 * units.liters
  units.imperialgallons = 4.54609 * units.liters
  units.litres = units.liter
  units.litres = units.liters
  units.drams = 3.5516328125 * units.milliliters
  units.dram = units.drams
  units.drachm = units.drams
  units.drachms = units.drams
  units.bits = 1
  units.nibbles = 4 * units.bits
  units.bytes = 8 * units.bits
  units.bit = units.bits
  units.nibble = units.nibbles
  units.byte = units.bytes
  units.kilobytes = 1024 * units.bytes
  units.megabytes = 1024 * units.kilobytes
  units.gigabytes = 1024 * units.megabytes
  units.terabytes = 1024 * units.gigabytes
  units.petabytes = 1024 * units.gigabytes
  units.exabytes = 1024 * units.petabytes
  units.zettabytes = 1024 * units.exabytes
  units.yottabytes = 1024 * units.zettabytes
  units.b = units.bytes
  units.kb = units.kilobytes
  units.mb = units.megabytes
  units.gb = units.gigabytes
  units.tb = units.terabytes
  units.pb = units.petabytes
  units.eb = units.exabytes
  units.zb = units.zettabytes
  units.yb = units.yottabytes
  units.gallon = units.gallons
  units.firkin = 9 * units.gallons
  units.kilderkin = 2 * units.firkin
  units.barrel = 2 * units.kilderkin
  units.hogshead = 54 * units.gallons
  units.firkins = units.firkin
  units.kilderkins = units.kilderkin
  units.barrels = units.barrel
  units.hogsheads = units.hogshead

  if args == 'units':
    u = [x for x in dir(units) if not x.startswith('_')]
    irc.msg(channel, ','.join(u))

  if args.startswith('!convert'):
    args = args[8:]

  split_args = args.split(' ')

  if len(split_args) < 3:
    irc.msg(channel, help())
    return

  tc = split_args.pop()
  to = split_args.pop()
  if to != 'to':
    tc = tc + to
  fc = split_args.pop()

  matches = re.findall('(^[0-9]+)(.+)?', fc)
  if matches and matches[0][1]:
    fa = matches[0][0]
    fc = matches[0][1]
  else:
    fa = split_args.pop()

  message = ''

  if fa not in maths.keys() and not fa.replace('.', '').isdigit():
    message = 'Invalid from amount. '

  if not fc.isalpha() or not tc.isalpha():
    message += 'Put a space between the numerator and the unit type. No non-alphabet characters. '

  if fc not in dir(units):
    message += 'Invalid type %s. ' % fc

  if tc not in dir(units):
    message += 'Invalid type %s. ' % tc

  if message:
    maybe = exchange.convert(args, user)
    if maybe and not maybe.startswith('!'):
      irc.msg(channel, maybe)
      return
    irc.msg(channel, 'Usage: Convert 10 cm to inches. ' + message)
    return message

  if fa in maths.keys():
    fa = maths[fa]
  else:
    fa = float(fa)

  converted = (getattr(units, fc) * fa / getattr(units, tc)).__str__().rstrip('1').rstrip('.')
  irc.msg(channel, '%s: %0.2f %s is %s %s.' % (user, fa, fc, converted, tc))


def help():
  return 'Usage: Convert 10 cm to inches.'
