from datetime import datetime
import dateutil.parser
import time

def getTimes():
  return {'epoch': time.time(), 'ascii': time.asctime()}

def run(irc, channel, user, args, useful):
  if not args:
    some_times = getTimes()
    irc.msg(channel, '%s: Current time is %s, or %s seconds past the epoch.' % (user, some_times['ascii'], some_times['epoch']))
  else:
    p = args.split(' ')
    if len(p) == 1:
      if p[0].isdigit():
        dt = datetime.fromtimestamp(float(p[0]))
      else:
        dt = datetime.now()
      irc.msg(channel, '%s: Time is %s, or %s seconds past the epoch.' % (user, dt.ctime(), p[0]))
    else:
      try:
        dt = dateutil.parser.parse(args)
      except ValueError as e:
        irc.msg(channel, '%s: Could not parse that time, try typing less like a twat.' % user)
        return
      ts = dt.strftime('%s')
      irc.msg(channel, '%s: Time is %s, or %s seconds past the epoch.' % (user, dt.ctime(), ts))
