def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, '%s: %s' % (user, help()))
    return 'solo'

  msg = ''
  for c in args:
    msg += str(ord(c)) + ' '

  irc.msg(channel, '%s: %s' % (user, msg))

def help():
  return 'Usage: ord <string>. Returns Python ordinals of string.'
