import plugins.libs.tfllib as tfl
import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  a = tfl.TFL()
  line = irc.firstArg(args).lower()
  if line == '':
    db = sqllib.sqllib()
    db.connect('/var/cache/remember.sql')

    results = a.Summary(db)
    for status in results:
      irc.msg(channel, status)
    return

  elif line == 'hammersmith':
    line = 'hammersmith and city'
  elif line == 'waterloo':
    line = 'waterloo and city'
  status = a.CachedLookup(line)

  if status:
    irc.msg(channel, '%s: The %s line status is: %s' % (user, line, status))
  else:
    irc.msg(channel, '%s: No information for line %s.' % (user, line))
