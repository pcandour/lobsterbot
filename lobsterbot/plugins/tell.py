def run(irc, channel, user, args, useful):
  if args == '' or ' ' not in args or user != irc.owner:
    irc.msg(user, '%s: What?' % user)
    return

  target, msg = args.split(' ', 1)

  irc.msg('#ed', "%s: %s" % (target, msg))

def help():
  return 'Usage: tell <user> <msg>.'
