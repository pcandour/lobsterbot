import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, '%s: What do you want me to remember?' % user)
    return

  import time

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  dt = int(time.time())

  db.execArgsAndCommit('INSERT INTO memory VALUES (null, ?, ?, ?)', (int(dt), user, str(args)))
  irc.msg(channel, "%s: Okay I'll remember: %s" % (user, str(args)))

def help():
  return 'Usage: Remember <something>. Remembers stuff.'
