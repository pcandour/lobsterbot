import plugins.libs.cachelib as cachelib
import shlex
import urllib.request as urllib2
from xml.etree import cElementTree

class TFL:

  def __init__(self, cache=None):
    if cache:
      self.c = cache
    else:
      self.c = cachelib.Cache()

  def UpdateStatus(self):
    opener = urllib2.build_opener()
    req = urllib2.Request('http://cloud.tfl.gov.uk/TrackerNet/LineStatus')
    req.add_header('User-Agent', '')
    try:
      d = opener.open(req).read()
    except:
      print ('Failed to parse TFL Tube data.')
      return 'Fail'
    p = cElementTree.fromstring(d)

    r = {}
    name = status = ''
    for x in p.iter():
      if x.tag == '{http://webservices.lul.co.uk/}Line' and 'Name' in x.keys():
        if name:
          r[name] = status
          status = ''
        name = x.items()[1][1].lower()
      elif status == '' and 'IsActive' in x.keys():
        status = x.items()[3][1]
      elif 'StatusDetails' in x.keys():
        details = x.items()[1][1]
        if details:
          status = '%s: %s' % (status, details)
    return r

  def CachedLookup(self, query):
    query = query.lower()
    timeout = 300
    value = self.c.getFromCache('tube-%s' % query, timeout=timeout)
    if value:
      return value

    data = self.UpdateStatus()
    if data:
      for line, status in data.iteritems():
        self.c.updateCache('tube-%s' % line, status)

    return data[query]

  def Summary(self, db):
    self.UpdateStatus()
    db.connect('/var/cache/remember.sql')
    db.execute('SELECT symbol, value FROM currcache WHERE symbol LIKE \'tube-%\' AND value != \'Good Service\';')
    results = db.fetchAll()
    if results:
      summary = []
      for line, status in results:
        summary.append('%s has %s' % (line.replace('tube-', '').title(), status))
    else:
      summary = ['All TFL lines have a good service.']
    return summary

