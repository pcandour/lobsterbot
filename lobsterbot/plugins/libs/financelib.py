import pickle
import shlex
import urllib.request as urllib2
import plugins.libs.cachelib as cachelib

class YahooFinance:

  def __init__(self, cache=None):
    if cache:
      self.c = cache
    else:
      self.c = cachelib.Cache()

  def BatchRetrieveAndParse(self, stocks):
    if not stocks:
      return {}
    opener = urllib2.build_opener()
    data = {}
    if type(stocks) == list:
      stock = ','.join(stocks)

    req = urllib2.Request('http://download.finance.yahoo.com/d/quotes.csv?s=%s&f=sl1ct1n0m6m8' % stock)
    req.add_header('User-Agent', '')
    try:
      body = opener.open(req).read().decode()
    except:
      body = 'Error'
    for d in body.split('\n'):
      if not d:
        continue
      s = shlex.shlex(d.strip(), posix=True)
      s.whitespace += ','
      s.whitespace_split = True
      p = list(s)
      data[p[0]] = p
    return data

  def RetrieveAndParse(self, stock):
    opener = urllib2.build_opener()
    req = urllib2.Request('http://download.finance.yahoo.com/d/quotes.csv?s=%s&f=sl1ct1n0m6m8' % stock)
    req.add_header('User-Agent', '')
    try:
      d = opener.open(req).read()
    except:
      d = 'Error'
    print(d)
    d = d.decode()
    # d = urllib2.urlopen('http://download.finance.yahoo.com/d/quotes.csv?s=%s&f=sl1ct1n0m6m8' % stock).read()
    s = shlex.shlex(d.strip(), posix=True)
    s.whitespace += ','
    s.whitespace_split = True
    p = list(s)
    # "GOOG",832.596,"0.00 - 0.00%","4:00pm","Google Inc.",+15.20%,+7.04%
    # ['GOOG', '784.99', '+2.57 - +0.33%', '11:54am', 'Google Inc.', '+15.20%', '+7.04%']
    return p

  def GetPriceAsString(self, p):
    if p[2] == 'N/A':
      return 'No such stock %s.' % p[0]

    return '%s (%s) was %s as of %s, a change of %s/%s/%s.' % (p[0], p[4], p[1], p[3], p[2], p[5], p[6])

  def BatchCachedLookup(self, stocks):
    timeout = 3600
    stock_data = {}
    need_stocks = []

    for stock in stocks:
      value = self.c.getFromCache('stocks-%s' % stock, timeout=timeout)
      if value:
        stock_data[stock] = pickle.loads(value)
      else:
        need_stocks.append(stock)

    data = self.BatchRetrieveAndParse(need_stocks)
    for stock, values in data.items():
      if values:
        self.c.updateCache('stocks-%s' % stock, pickle.dumps(values))
      stock_data[stock] = values

    return stock_data

  def CachedLookup(self, stock):
    timeout = 3600
    value = self.c.getFromCache('stocks-%s' % stock, timeout=timeout)
    if value:
      return pickle.loads(value)

    data = self.RetrieveAndParse(stock)
    if data[3] == 'N/A':
      orig_stock = stock
      stock = stock + '.L'
      values = self.c.getFromCache('stocks-%s' % stock, timeout=3600)
      if values:
        return pickle.loads(values)

      data = self.RetrieveAndParse(stock)

    if data:
      self.c.updateCache('stocks-%s' % stock, pickle.dumps(data))

    return data

  def CachedStringLookup(self, stock):
    d = self.CachedLookup(stock)
    if d[2] == 'N/A':
      return 'Stock not found.'
    return self.GetPriceAsString(d)
