class VimeoError(Exception):

  def __init__(self, error):
    self.error = error

  def __str__(self):
    return self.error


class VimeoLib(object):
  def Lookup(self, key):
    import json
    import urllib.request as urllib2

    url = 'http://vimeo.com/api/v2/video/%s.json' % key
    try:
      resp = urllib2.urlopen(url).read()
    except urllib2.HTTPError as e:
      raise VimeoError(e)

    v = json.loads(resp.decode())
    if 'title' in v[0]:
      return '%s (%ss)' % (str(v[0]['title']), str(v[0]['duration']))
    else:
      print ('Vimeo error:', v)
      raise VimeoError('Parsing error.')
