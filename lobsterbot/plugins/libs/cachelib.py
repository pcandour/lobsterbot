import plugins.libs.sqllib as sqllib

class Cache():
  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')
  db.setRowFactory()

  def updateCache(self, key, value):
    self.db.execArgsAndCommit(u'INSERT OR REPLACE INTO currcache (symbol, datetime, value) VALUES (?, strftime(\'%s\', \'now\'), ?)', (key, value))

  def getFromCache(self, key, timeout=86400):
    self.db.executeArgs(u'SELECT value FROM currcache WHERE symbol=? AND datetime > strftime(\'%s\', \'now\')-?', (key, timeout))
    res = self.db.fetchOne()
    if res:
      return res[0]
    return ''
