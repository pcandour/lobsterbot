import plugins.libs.sqllib as sqllib

class Keyvalue():
  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  def updateKeyvalue(self, table, key, value):
    key = '%s-%s' % (table, key)
    self.db.execArgsAndCommit('INSERT OR REPLACE INTO keyvalue (key, value) VALUES (?, ?)', (key, value))

  def getKeyvalue(self, table, key):
    key = '%s-%s' % (table, key)
    self.db.executeArgs('SELECT value FROM keyvalue WHERE key=?', (key,))
    res = self.db.fetchOne()
    if res:
      return res[0]
    return None
