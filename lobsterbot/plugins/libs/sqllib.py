import sqlite3

class sqllib(object):
  db = "/var/cache/remember.sql"
  logfile = "/var/cache/c-log.txt"
  conn = None
  cursor = None
  result = None
  last_query = None
  debug = False

  def log(self, text):
    fp = open(self.logfile, 'a')
    fp.write(text + "\n")
    if (text.startswith('Error')):
      fp.write('Last query: %s\n' % self.last_query)

  def setLogfile(self, filename):
    self.logfile = filename

  def connect(self, db=None):
    if db is None and self.db is None:
      self.log("No database specified.")
      return None

    if db:
      if self.debug:
        self.log('Connecting to passed in db %s' % db)
      self.db = db
    else:
      if self.debug:
        self.log('Connecting to %s' % self.db)

    try:
      self.conn = sqlite3.connect(self.db, check_same_thread=False)
      self.cursor = self.conn.cursor()
    except sqlite3.Error as e:
      self.log("Error: %s" % e)
      return 0

    return 1

  def setDb(self, db):
    if self.debug:
      self.log('Set db to %s' % db)

    self.db = db

  def execute(self, query):
    if self.debug:
      self.log("Query: %s" % query)
    self.last_query = query

    try:
      self.result = self.cursor.execute(query)
      return 1
    except sqlite3.Error as e:
      if self.conn:
        self.rollback()
      self.log("Error: %s" % e)

    return 0

  def executeArgs(self, query, args):
    if self.debug:
      self.log("Query: %s ? %s" % (query, args))
    self.last_query = query

    try:
      self.result = self.cursor.execute(str(query), args)
      return 1
    except sqlite3.Error as e:
      if self.conn:
        self.rollback()
      self.log("Error: %s" % e)

    return 0

  def executeMany(self, query, data):
    try:
      if self.debug:
        self.log("ManyQuery: %s" % query)
      self.last_query = query
      self.result = self.cursor.executemany(query, data)
    except sqlite3.Error as e:
      if self.conn:
        self.rollback()
      self.log("Error: %s" % e)

  def commit(self):
    self.conn.commit()

  def rollback(self):
    self.conn.rollback()

  def execAndCommit(self, query):
    if self.execute(query):
      self.commit()
      return 0
    return 1

  def execArgsAndCommit(self, query, args):
    if self.executeArgs(query, args):
      self.commit()
      return 0
    return 1

  def close(self):
    self.cursor.close()

  def mydecode(self, arg):
    if isinstance(arg, bytes):
      return arg.decode()
    return arg

  def fetchAll(self):
    return self.cursor.fetchall()

  def fetchOne(self):
    cur = self.cursor.fetchone()
    if not cur:
      return None
    return tuple([self.mydecode(x) for x in cur])

  def lastInsertId(self):
    return self.cursor.lastrowid()

  def fieldNames(self, table):
    fields = []
    self.cursor.execute('PRAGMA table_info(%s)' % table)
    for field in self.fetchAll():
      fields.append(field[1])

    return fields

  def tableExists(self, table):
    if self.debug:
      self.log("tableExists?: %s" % table)

    self.execute("SELECT name FROM sqlite_master WHERE name='%s'" % table)
    try:
      r = self.fetchOne()
    except sqlite3.Error as e:
      self.log("Error: %s" % e)
      return False

    if self.debug:
      if r:
        self.log('Table existed')
      else:
        self.log('Table did not exist')

    return (not not r)

  def rowCount(self):
    return self.cursor.rowcount

  def setRowFactory(self):
    self.conn.row_factory = sqlite3.Row
#    self.conn.text_factory = str
    self.cursor = self.conn.cursor()
