import soundcloud as scc

class SoundCloud:
  def __init__(self):
    import plugins.libs.sqllib as sqllib
    db = sqllib.sqllib()
    db.connect()
    db.execute('SELECT key FROM apikeys WHERE name = "soundcloudid"')
    res = self.db.fetchOne()
    if res:
      self.client = scc.Client(client_id=res[0])
    else:
      self.client = scc.Client(client_id='CLIENTID')

  def topthree(self):
    tracks = self.client.get('/tracks', order='hotness', limit=3)
    return tracks

  def geturl(self, url):
    track = self.client.get('/resolve', url=url)
    if track.error:
      return {'error': track.error}

    title = str(track.fields()['title'])
    user = str(track.fields()['user']['username'])

    return {'user': user, 'title': title}

  def search(self, query):
    track = self.client.get('/tracks', q=query)
    if track.error:
      return {'error': track.error}

    track = track.pop()
    title = str(track.obj['title'])
    user = str(track.obj['user']['username'])
    url = str(track.obj['permalink_url'])

    return {'user': user, 'title': title, 'url': url}
