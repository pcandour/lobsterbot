import urllib.request as urllib2
import plugins.libs.cachelib as cachelib
import html.parser
import json

class Imgur(object):

  def __init__(self, cache=None):
    import plugins.libs.sqllib as sqllib
    db = sqllib.sqllib()
    db.connect()
    db.execute('SELECT key FROM apikeys WHERE name = "imgurclient"')
    res = self.db.fetchOne()
    if res:
      clientid = res[0]
      self.header = {
        'Authorization': 'Client-ID ' + self.clientid,
        'Content-Type': 'application/json'
      }
    else:
      self.header = {}

    if not cache:
      self.c = cachelib.Cache()
    else:
      self.c = cache


  def LookupTitle(self, url):
    if ('/gallery/' in url):
      key = url.rpartition('/')[2]
      apicall = 'album'

      if key:
        value = self.c.getFromCache('imgur-'+key, timeout=608400)
        if value:
          return value

      json = self.QueryApi(apicall, key)
      if json == None:
        print("Invalid or unknown imgur API key")
        return

      title = json['data']['title']
      if (title == ""):
        title = json['data']['description']
        
      if json['data']['images_count'] > 0:
        desc = json['data']['images'][0]['description']
        if (desc == ""):
          desc = json['data']['images'][0]['title']
        if (desc != ""):
          title = '%s: %s' % (title, desc)
      self.c.updateCache(key, title)
      return title
    return "Unknown imgur type"
    

  def QueryApi(self, apicall, gallery):
    if self.header == {}:
      return None

    url = 'https://api.imgur.com/3/%s/%s' % (apicall, gallery)

    request = urllib2.Request(url, headers=self.header)
    resp = urllib2.urlopen(request).read()
    return json.loads(resp.decode())
