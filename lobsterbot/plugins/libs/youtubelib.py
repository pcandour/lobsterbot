from apiclient.discovery import build
from optparse import OptionParser

# Set DEVELOPER_KEY to the "API key" value from the "Access" tab of the
# Google APIs Console http://code.google.com/apis/console#access
# Please ensure that you have enabled the YouTube Data API for your project.

def __init__(self):
  import plugins.libs.sqllib as sqllib
  db = sqllib.sqllib()
  db.connect()
  db.execute('SELECT key FROM apikeys WHERE name = "youtubekey"')
  res = self.db.fetchOne()
  if res:
    self.DEVELOPER_KEY = res[0]
  else:
    self.DEVELOPER_KEY = "DEVELOPER_KEY"

  self.YOUTUBE_API_SERVICE_NAME = "youtube"
  self.YOUTUBE_API_VERSION = "v3"
  

def get_length(yt, id):
  search_response = yt.videos().list(
    id=id,
    part="contentDetails",
  ).execute()
  duration = search_response["items"][0]["contentDetails"]["duration"]
  duration = duration[2:].lower()
  return duration


def youtube_get_video(video_id):
  youtube = build(self.YOUTUBE_API_SERVICE_NAME, self.YOUTUBE_API_VERSION,
    developerKey=self.DEVELOPER_KEY)

  video_response = youtube.videos().list(
      id=video_id,
      part='snippet,contentDetails'
      ).execute()

  videos = []

  video_result = video_response.get("items", [])
  if not video_result:
    return
  video_result = video_result[0]
  duration = video_result["contentDetails"]["duration"]
  duration = duration[2:].lower()
  return (video_result["snippet"]["title"],
          duration)


def youtube_search(query, maxResults=1, safeSearch="none", search_type="video"):
  youtube = build(self.YOUTUBE_API_SERVICE_NAME, self.YOUTUBE_API_VERSION,
    developerKey=self.DEVELOPER_KEY)

  search_response = youtube.search().list(
    q=query,
    part="id,snippet",
    maxResults=maxResults,
    safeSearch=safeSearch,
    type=search_type
  ).execute()

  videos = []
  channels = []
  playlists = []

  for search_result in search_response.get("items", []):
    if search_result["id"]["kind"] == "youtube#video":
      videos.append((search_result["snippet"]["title"],
                     search_result["id"]["videoId"],
                     get_length(youtube, search_result["id"]["videoId"])))
    elif search_result["id"]["kind"] == "youtube#channel":
      channels.append((search_result["snippet"]["title"],
                       search_result["id"]["channelId"]))
    elif search_result["id"]["kind"] == "youtube#playlist":
      playlists.append((search_result["snippet"]["title"],
                        search_result["id"]["playlistId"]))

  return({'videos': videos, 'channels': channels, 'playlists': playlists})
