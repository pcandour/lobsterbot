import urllib
import urllib.request as urllib2
import json

class digitalocean(object):

  def __init__(self, name=None):
    self.header = {
        'Authorization': 'Bearer BEARERTOKEN',
        'Content-Type': 'application/json'}
    self.api = 'https://api.digitalocean.com/v2'
    if name:
      self.name = name
    else:
      self.name = 'minecraft'

  def call_api(self, call, data=None, method='GET'):
    if data:
      request = urllib2.Request('%s/%s' % (self.api, call), data, headers=self.header)
    else:
      request = urllib2.Request('%s/%s' % (self.api, call), headers=self.header)
    if method != 'GET':
      request.get_method = lambda: method
    resp = urllib2.urlopen(request).read()
    try:
      return json.loads(resp)
    except:
      return resp

  def action(self, host_id, data=None):
    return self.call_api('droplets/%s/actions' % host_id, data=data)['action']['status']

  def droplets(self, tag='gaming'):
    return self.call_api('droplets?tag_name=%s' % tag)

  def get_status(self):
    host = self.name
    hosts = self.call_api('droplets')
    status = [
        {'id': x['id'], 'status': x['status'], 'locked': x['locked']}
            for x in hosts['droplets'] if x['name'] == host]
    if len(status):
      return status[0]
    else:
      return {'id': 0, 'status': 'down', 'locked': False}

  def shutdown(self, host=None):
    status = self.get_status()
    if not status['id']:
      return 'Not found.'
    if status['locked']:
      return 'Locked'
    if status['status'] != 'active':
      return 'Not active'
    return self.action(status['id'], '{"type": "shutdown"}')

  def boot(self, host=None):
    status = self.get_status()
    if not status['id']:
      return 'Not found.'
    if status['locked']:
      return 'Locked'
    if status['status'] != 'active':
      return 'Active'
    return self.action(status['id'], '{"type": "power_on"}')

  def create_from_snapshot(self, snapshot):
    if not snapshot:
      return 'Must specify a snapshot id.'
    status = self.get_status()
    if status['id']:
      return 'Droplet already exists.'
    create = (
    '{"name": "' + self.name + '",'
    '"region": "lon1",'
    '"size": "8gb",'
    '"image": "' + snapshot + '"}')
    return self.call_api('droplets', data=create)

  def destroy_droplet(self):
    status = self.get_status()
    if status['locked'] or status['status'] == 'active':
      return 'Error: Locked or active.'
    self.call_api('droplets/%s' % status['id'], method='DELETE')
    return 'Droplet deleted.'

  def get_snapshots(self):
    snapshots = self.call_api('snapshots')
    return [x for x in snapshots['snapshots'] if x['name'] == self.name]

  def create_snapshot(self):
    status = self.get_status()
    if not status['id']:
      return 'Not found.'
    if status['locked']:
      return 'Locked'
    if status['status'] == 'active':
      return 'Not shutdown'
    return self.action(status['id'], '{"type": "snapshot", "name": "' + self.name + '"}')

  def delete_snapshot(self, s_id):
    snapshots = self.get_snapshots()
    if s_id and s_id in [x['id'] for x in snapshots]:
      return self.call_api('snapshots/%s' % s_id, method='DELETE')
    return 'Not found'

  def assign_floating_ip(self):
    status = self.get_status()
    if not status['id'] or status['locked'] or not status['status'] == 'active':
      return 'Failed to assign floating IP.'
    d_id = status['id']
    data = '{"type": "assign", "droplet_id": %d}' % d_id
    return self.call_api('floating_ips/IPADDRESS/actions', data=data)
