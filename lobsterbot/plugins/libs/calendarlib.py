import atom
import oauthlib
import gdata.calendar.data
import gdata.calendar.client
import time

class Calendar(oauthlib.OAuth):

  def __init__(self):
    self.client = gdata.calendar.client.CalendarClient(source='LobsterBot')

  def ListCalendars(self):
    feed = self.client.GetOwnCalendarsFeed()
    calendars = []
    for a_calendar in feed.entry:
      calendars.append(a_calendar.title.text)
    return calendars

  def GetTitle(self):
    feed = self.client.GetOwnCalendarsFeed()
    return feed.title.text

  def ReturnAllEvents(self):
    feed = self.client.GetCalendarEventFeed()
    events = []
    for event in feed.entry:
      events.append(event.title.text)
    return events

  def QuickAdd(self, event_text):
    event = gdata.calendar.data.CalendarEventEntry()
    event.content = atom.data.Content(text=event_text)
    event.quick_add = gdata.calendar.data.QuickAddProperty(value='true')

    try:
      e = self.client.InsertEvent(event)
    except RedirectError:
      e = self.client.InsertEvent(event)

    return e

  def EventInfo(self, event):
    if event.recurrence:
      r = event.recurrence.text.split('\n')
      start = r[0].split(':')[1]
      end = r[2].split('=')[1]
    else:
      start = event.when[0].start[0:16].replace('T', ' ')
      end = event.when[0].end[0:16].replace('T', ' ')
    return {'id': event.id.text,
            'title': event.title.text,
            'start': start,
            'end': end,
            'who': event.who[0].value,
            'edit': event.GetEditLink().href,
            'html': event.GetHtmlLink().href}

  def DeleteEvent(self, event):
    return self.client.DeleteEvent(event.GetEditLink().href)

  def FullTextQuery(self, text_query):
    if not text_query:
      return []
    query = gdata.calendar.client.CalendarEventQuery(text_query=text_query)
    feed = self.client.GetCalendarEventFeed(q=query)

    return feed.entry

  def EventInfoAsString(self, event):
    e = self.EventInfo(event)

    if e['end'] == 'YEARLY':
      return '%s every year on %s (%s)' % (e['title'], e['start'], e['id'])
    elif e['end'] == 'MONTHLY;BYMONTHDAY':
      return '%s every month from %s (%s)' % (e['title'], e['start'], e['id'])
    else:
      return '%s from %s to %s (%s)' % (e['title'], e['start'], e['end'], e['id'])
