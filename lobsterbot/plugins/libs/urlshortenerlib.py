import httplib2
import simplejson as json
import plugins.libs.cachelib as cachelib

class UrlShortener():

  def shorten(self, url, cache=None):
    if not cache:
      cache = cachelib.Cache()

    short_url = cache.getFromCache('shorten-%s' % url, timeout=8640000)
    if short_url:
      return short_url

    data = {'longUrl': url}
    headers = {'Content-Type': 'application/json' }
    h = httplib2.Http('.cache')
    try:
      headers, response = h.request('https://www.googleapis.com/urlshortener/v1/url', 'POST', json.dumps(data), headers)
      short_url = json.loads(response)['id']
    except:
      return 'Error'

    cache.updateCache('shorten-%s' % url, short_url)

    return short_url
