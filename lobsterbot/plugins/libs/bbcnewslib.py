import plugins.libs.cachelib as cachelib
import urllib.request as urllib2
import html.parser

class BbcNews:

  def __init__(self, cache=None):
    if not cache:
      self.c = cachelib.Cache()
    else:
      self.c = cache

  def GetTitle(self, url):
    try:
      d = urllib2.urlopen(url).read()
    except urllib2.HTTPError as e:
      return None
    if isinstance(d, bytes):
      d = d.decode()
    statitle = d.index('<title>') + 7
    endtitle = d.index('</title>')
    title = d[statitle:endtitle]
    h = html.parser.HTMLParser()
    title = h.unescape(title.strip().replace('&#10;', ' '))

    return title

  def CachedTitleLookup(self, url):
    # http://www.bbc.co.uk/news/business-20336898
    if 'bbc.co' in url:
      key = 'bbcnews-%s' % url.rpartition('/')[2]
    elif 'imgur.com' in url:
      import plugins.libs.imgurlib as imgurlib
      i = imgurlib.Imgur()
      return i.LookupTitle(url)
    else:
      key = 'urlcache-%s' % url

    if key:
      value = self.c.getFromCache(key, timeout=608400)
      if value:
        return value

    title = self.GetTitle(url)
    self.c.updateCache(key, title)
    return title
