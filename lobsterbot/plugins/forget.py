import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, 'Forget what?')
    return

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  arg = irc.firstArg(args)
  if arg.isdigit():
    arg = int(arg) - 1
    db.executeArgs('SELECT id, datetime, memory FROM memory WHERE user=? AND id=?', (user, arg+1))
    res = db.fetchAll()

    if len(res) == 0:
      irc.msg(channel, '%s: I don\t know that reference.' % user)
      return

    res = res[0]
    irc.msg(channel, "%s: Forgetting <%s> [%s] %s." % (user, res[0].__str__(), res[1].__str__(), res[2].__str__()))
    db.execArgsAndCommit('DELETE FROM memory WHERE user=? AND id=?', (user, arg+1))
    return

  irc.msg(channel, 'I don\'t understand what you want.')

def help():
  return 'Usage: forget <item number>. Forget your stored memories.'
