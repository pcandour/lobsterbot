import plugins.libs.cachelib as cachelib
import datetime
import json
import urllib.request as urllib2

def run(irc, channel, user, args, useful):
  if args == '' or args == '!weather':
    import plugins.libs.sqllib as sqllib
    db = sqllib.sqllib()
    db.connect()

    db.execute('SELECT key FROM apikeys WHERE name = "weather"')
    res = self.db.fetchOne()
    if res:
      key = res[0]
    else:
      key = "KEY"

    db.executeArgs('SELECT location FROM userinfo WHERE user=?', (user, ))
    res = db.fetchOne()
    args = str(res[0])

  if args.startswith('!weather'):
    args = args[9:]

  if not args:
    return

  location = args.replace(' ', ',')

  cache = cachelib.Cache()
  report = cache.getFromCache('weather-%s' % location, timeout=1800)
  if report:
    irc.msg(channel, '%s (cached)' % report)
    return 'Weather cached'

  url = 'https://api.openweathermap.org/data/2.5/forecast?q=%s&units=metric&appid=%s' % (location, key)

  try:
    resp = urllib2.urlopen(url).read()
  except urllib2.HTTPError as e:
    raise WeatherError(e)

  if isinstance(resp, bytes):
    resp = resp.decode()

  p = json.loads(resp)
# {"dt":1487246400,"main":{"temp":286.67,"temp_min":281.556,"temp_max":286.67,"pressure":972.73,"sea_level":1046.46,"grnd_level":972.73,"humidity":75,"temp_kf":5.11},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"clouds":{"all":0},"wind":{"speed":1.81,"deg":247.501},"sys":{"pod":"d"},"dt_txt":"2017-02-16 12:00:00"}
  
  a = p['list'][1]
  midday = 0
  for x in range(2, 8):
    if p['list'][x]['dt_txt'].endswith('12:00:00'):
      midday = x
      break
  b = p['list'][midday]
  c = p['list'][midday+8]
  day = datetime.datetime.fromtimestamp(c['dt']).strftime('%A')

  #report = 'Weather for %s: Today at %s it will be %s and %0.1fc-%0.1fc. Tomorrow at midday it will be %s and %0.1fc-%0.1fc, and %s at midday it will be %s and %0.1fc-%0.1fc.' % (p['city']['name'], a['dt_txt'][11:], a['weather'][0]['main'], a['main']['temp_min'], a['main']['temp_max'], b['weather'][0]['main'], b['main']['temp_min'], b['main']['temp_max'], day, c['weather'][0]['main'], c['main']['temp_min'], c['main']['temp_max'])
  report = 'Weather for %s, %s (pop: %d): Today at %s it will be %s and %0.1fc-%0.1fc. Tomorrow at midday it will be %s and %0.1fc, and %s at midday it will be %s and %0.1fc.' % (p['city']['name'], p['city']['country'], p['city']['population'], a['dt_txt'][11:], a['weather'][0]['main'], a['main']['temp_min'], a['main']['temp_max'], b['weather'][0]['main'], b['main']['temp'], day, c['weather'][0]['main'], c['main']['temp'])

  cache.updateCache('weather-%s' % location, report)
  irc.msg(channel, report)

def help():
  return 'Usage: weather <location>. Example: weather amsterdam,netherlands, or weather london,uk, or weather Hell US'
