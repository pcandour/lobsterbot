import re
import urllib2
import time

def getLinks(html):
  return re.findall('class="waveform" src="[^"]+\/([^_]+)_', html)

def getSet(html):
  l = re.findall('<div class="actions"><a href="([^"]+)', html)
  c = len(getLinks(html))
  l = l[c:-1]
  links = []
  for url in l:
    time.sleep(1)
    html = urllib2.urlopen('http://soundcloud.com%s' % url).read()
    links.append(getLinks(html)[0])
  return set(links)

def run(irc, channel, user, args, useful):
  url = irc.firstArg(args)
  html = urllib2.urlopen(url).read()
  links = getLinks(html)

  m = ''
  for l in links:
    m = m + 'http://media.soundcloud.com/stream/%s.mp3 ' % l
    if len(m) > 150:
      irc.msg(user, m)
      m = ''
  irc.msg(user, m)

  if '/sets/' in url:
    links = getSet(html)

  m = ''
  for l in links:
    m = m + 'http://media.soundcloud.com/stream/%s.mp3 ' % l
    if len(m) > 150:
      irc.msg(user, m)
      m = ''
      time.sleep(1)
  irc.msg(user, m)

def help():
  return 'Usage: downcloud <track url>.'
