def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, help())
    return

  chan = irc.firstArg(args)
  irc.msg(user, 'Okay, joining %s.' % chan)
  irc.join(chan)

def owner():
  return 1

def help():
  return 'Usage: channeljoin <channel>.'
