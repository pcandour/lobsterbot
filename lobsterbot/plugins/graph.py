import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, '%s: What do you want me to add to the graph?' % user)
    return

  a = args.split('+')

  if len(a) > 1:
    value = a[1]
  else:
    value = 1

  if len(a) > 2:
    dt = a[2]
  else:
    dt = 'now'

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  db.execArgsAndCommit('INSERT INTO graphdata (user, key, datetime, value) VALUES (?, ?, strftime(\'%s\', ?), ?)', (user, str(a[0]), dt, value))
  irc.msg(channel, "%s: Okay I'll graph %s with a value of %s at %s." % (user, str(a[0]), value, dt))

def help():
  return 'Usage: graph <key>[+value][+datetime]. Datetime is 2012-10-02 plus optional T10:00.'
