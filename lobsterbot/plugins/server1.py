import plugins.libs.digitalocean as digitalocean

def run(irc, channel, user, args, useful):
  do = digitalocean.digitalocean()

  if args == 'status':
    status = do.get_status()
    if 'status' in status:
      if status['id'] == 0:
        irc.msg(channel, 'Droplet does not exist.')
        return
      msg = '%s and ' % status['status']
      if status['locked']:
        msg += 'locked.'
      else:
        msg += 'unlocked.'
      irc.msg(channel, msg)
    else:
      irc.msg(channel, 'Server status unknown, error.')

  elif args == 'start':
    irc.msg(channel, do.boot())

  elif args == 'shutdown':
    irc.msg(channel, do.shutdown())

  elif args == 'snapshots':
    snapshots = do.get_snapshots()
    for s in snapshots:
      irc.msg(channel, 'Snapshot %s created at %s.' % (s['id'], s['created_at']))

  elif args.startswith('snapshot del'):
    s_id = args[13:]
    try:
      do.delete_snapshot(s_id)
      irc.msg(channel, 'Snapshot deleted.')
    except:
      irc.msg(channel, 'Error deleting snapshot.')

  elif args == 'snapshot create':
    msg = do.create_snapshot()
    irc.msg(channel, str(msg))

  elif args.strip() == 'create':
    irc.msg(channel, 'Finding snapshots.')
    snapshots = do.get_snapshots()
    max_snap = max([x['id'] for x in snapshots])
    irc.msg(channel, 'Found snapshot: %s' % max_snap)
    msg = do.create_from_snapshot(max_snap)
    if 'droplet' in msg:
      if 'status' in msg['droplet']:
        irc.msg(channel, 'Creating droplet: Success.')
        return
    irc.msg(channel, 'Creating droplet error: %s' % msg)

  elif args.startswith('create'):
    s_id = args[7:]
    msg = do.create_from_snapshot(s_id)
    if 'droplet' in msg:
      if 'status' in msg['droplet']:
        irc.msg(channel, 'Creating droplet: Success.')
        return
    irc.msg(channel, 'Creating droplet error: %s' % msg)

  elif args == 'destroy':
    irc.msg(channel, str(do.destroy_droplet()))

  elif args == 'assign ip':
    status = do.assign_floating_ip()
    if 'action' in status:
      if 'status' in status['action']:
        irc.msg(channel, 'Assign IP: %s' % status['action']['status'])
    irc.msg(channel, msg)

  else:
    irc.msg(channel, help())

def help():
  return 'Usage: server status|start|shutdown|create|destroy|assign ip. Or server snapshots|snapshot del|snapshot create.'
