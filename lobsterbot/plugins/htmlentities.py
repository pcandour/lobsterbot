from html5lib.constants import entities

def decode(bytes):
  try:
    text = bytes.decode('utf-8')
  except UnicodeDecodeError:
    try:
      text = bytes.decode('iso-8859-1')
    except UnicodeDecodeError:
      text = bytes.decode('cp1252')
  return text

def encode(bytes):
  try:
    text = bytes.encode('utf-8')
  except UnicodeEncodeError:
    try:
      text = bytes.encode('iso-8859-1')
    except UnicodeEncodeError:
      text = bytes.encode('cp1252')
  return text

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(user, 'What?')
    return

  if ' ' in args:
    chars = args.split(' ')[0]
  else:
    chars = args

  if chars.startswith('&'):
    chars = chars[1:]
  if chars.endswith(';'):
    chars = chars[:-1]

  if chars in entities:
    irc.msg(channel, '%s: ' % user + encode(entities[chars]))
  elif decode(chars) in entities.values():
    d = decode(chars)
    for k, v in entities.iteritems():
      if v == d:
        irc.msg(channel, '%s: &%s;' % (user, k))
        break
  else:
    irc.msg(channel, '%s: Unknown char/entity.' % user)

def help():
  return 'Usage: htmlentities <character>.'
