import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  import time

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  if args == '':
    # Do suggestion
    db.execute('SELECT url FROM music ORDER BY RANDOM() limit 3')
    urls = []
    results = db.fetchAll()
    for res in results:
      urls.append(str(res[0]))
    irc.msg(channel, '%s: %s' % (user, ' '.join(urls)))
    return

  (cmd, url) = irc.splitArgs(args)

  if url == '':
    irc.msg(channel, "%s: You need to say 'music add <URL>'." % user)
    return

  if cmd in [ 'add', 'remember' ]:
    db.executeArgs('SELECT 1 FROM music WHERE url=?', (str(url), ))
    res = db.fetchOne()
    if res:
      irc.msg(channel, "%s: Already have that track." % user)
    else:
      dt = int(time.time())

      import re
      ytids = re.findall('https?:\/\/www.youtube.com\/[^ ]*v=([-_A-Za-z0-9]{11})', args)
      if not ytids:
        ytids = re.findall('https?:\/\/youtu.be\/[^ ]*([-_A-Za-z0-9]{11})', args)
      scids = re.findall('(http:\/\/soundcloud.com\/[^ ]*)', args)

      if ytids:
        import plugins.libs.youtubelib as youtubelib
        client = youtubelib.Youtube()
        titles = list()

        feed = client.search(ytids[0])
        if type(feed) == type(list()):
          for entry in feed.entry:
            title = client.title(entry)
        else:
          title = client.title(feed)
        irc.msg(channel, "%s: New music added: %s" % (user, str(title)))
      elif scids:
        import plugins.libs.soundcloudlib as soundcloudlib
        client = soundcloudlib.SoundCloud()
        track = client.geturl(scids[0])
        if 'error' in track:
          irc.msg(channel, '%s: Error with that track: %s' % (user, track['error']))
          return 'music add soundcloud error %s' % scid
        else:
          irc.msg(channel, '%s: New music added: %s by %s' % (user, track['title'], track['user']))

      else:
        irc.msg(channel, "%s: New music added: %s" % (user, str(url)))

      db.execArgsAndCommit('INSERT INTO music VALUES (null, ?, ?, ?, \'F\')', (int(dt), user, str(url)))
    return

  if cmd in [ 'forget', 'remove', 'delete' ]:
    db.execArgsAndCommit('UPDATE music SET deleted=\'T\' WHERE url=?', (str(url), ))
    irc.msg(channel, "%s: Track removed: %s" % (user, str(url)))
    return

  if cmd in [ 'info', 'information' ]:
    db.executeArgs('SELECT user, datetime, deleted FROM music WHERE url=?', (str(url), ))
    result = db.fetchOne()
    if result[2] == 'T':
      irc.msg(channel, '%s: %s added by %s at %s but was deleted.' % (user, url, str(result[0]), time.ctime(int(result[1]))))
    else:
      irc.msg(channel, '%s: %s added by %s at %s.' % (user, url, str(result[0]), time.ctime(int(result[1]))))
    return

def help():
  return 'Usage: Music [(add/forget) <url>]. Returns a random music track, or adds a new one.'
