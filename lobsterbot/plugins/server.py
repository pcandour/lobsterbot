import time
import plugins.libs.digitalocean as digitalocean

def wait_unlocked(do):
  time.sleep(1)
  while is_locked(do):
    time.sleep(10)

def wait_active(do):
  time.sleep(1)
  while is_locked(do) or not is_active(do):
    time.sleep(10)

def wait_down(do):
  time.sleep(1)
  while not is_down(do):
    time.sleep(10)

def wait_on(do):
  time.sleep(1)
  while not is_up(do):
    time.sleep(10)

def wait_destroyed(do):
  time.sleep(1)
  while not is_destroyed(do):
    time.sleep(5)

def is_active(do, status=None):
  if not status:
    status = do.get_status()
  if status['status'] == 'active':
    return True

def is_destroyed(do, status=None):
  if not status:
    status = do.get_status()
  if status['id'] == 0:
    return True

def is_down(do, status=None):
  if not status:
    status = do.get_status()
  if status['status'] == 'off' and not status['locked'] and status['id'] != 0:
    return True

def is_up(do, status=None):
  if not status:
    status = do.get_status()
  if status['id'] != 0 and status['status'] == 'active':
    return True

def is_locked(do, status=None):
  if not status:
    status = do.get_status()
  return status['locked']

def run(irc, channel, user, args, useful):
  do = digitalocean.digitalocean()

  if args == 'status':
    status = do.get_status()
    if 'status' in status:
      if status['id'] == 0:
        irc.msg(channel, 'Droplet does not exist.')
        return
      msg = '%s and ' % status['status']
      if status['locked']:
        msg += 'locked.'
      else:
        msg += 'unlocked.'
      irc.msg(channel, msg)
    else:
      irc.msg(channel, 'Server status unknown, error.')

  elif args == 'start':
    if not is_destroyed(do):
      irc.msg(channel, 'Server already up.')
      return
    irc.msg(channel, 'Waiting for unlock.')
    wait_unlocked(do)
    irc.msg(channel, 'Finding snapshots.')
    snaps = do.get_snapshots()
    if not snaps:
      irc.msg(channel, 'Failed to find snapshots')
      return
    max_snap = max([x['id'] for x in snaps])
    if not max_snap:
      irc.msg(channel, 'Failed to understand snapshot response.')
      return
    snap_date = [x['created_at'] for x in snaps if x['id'] == max_snap][0]
    irc.msg(channel, 'Creating from snapshot %s on %s.' % (max_snap, snap_date))

    msg = do.create_from_snapshot(max_snap)
    if 'droplet' not in msg:
      irc.msg(channel, 'Failed to create from snapshot. %s' % str(msg))
      return
    if 'status' not in msg['droplet']:
      irc.msg(channel, 'Failed to create from snapshot. %s' % str(msg))
      return
    irc.msg(channel, 'Waiting for server to start...')
    time.sleep(60)
    wait_active(do)
    irc.msg(channel, 'Assigning IP address.')
    status = do.assign_floating_ip()
    if 'action' in status:
      if 'status' in status['action']:
        irc.msg(channel, 'Server should now be ready!')
      else:
        irc.msg(channel, 'Error somewhere.')
    else:
      irc.msg(channel, 'Error somewhere.')

  elif args == 'shutdown':
    if is_destroyed(do):
      irc.msg(channel, 'Server already destroyed.')
      return
    irc.msg(channel, 'Shutting down...')
    wait_unlocked(do)
    irc.msg(channel, do.shutdown())
    wait_down(do)
    irc.msg(channel, 'Creating snapshot...')
    irc.msg(channel, do.create_snapshot())
    wait_unlocked(do)
    irc.msg(channel, 'Destroying droplet.')
    irc.msg(channel, str(do.destroy_droplet()))

  else:
    irc.msg(channel, help())

def help():
  return 'Usage: server status|start|shutdown.'
