import re
import plugins.libs.cachelib as cachelib
import plugins.libs.vimeolib as vimeolib

def run(irc, channel, user, args, useful):

  if 'vimeo.com' in args:
    keys = re.findall('https?:\/\/vimeo.com\/([0-9]{8})', args)
  elif len(args) == 8 and args.isdigit():
    keys = []
    keys[0] = args
  else:
    return

  titles = []
  v = vimeolib.VimeoLib()
  c = cachelib.Cache()

  for key in keys:
    title = c.getFromCache('vimeo-%s' % key, timeout=8640000)

    if not title:
      try:
        title = v.Lookup(key)
      except VimeoError as e:
        irc.msg(channel, '%s: %s' % (user, e))

    c.updateCache('vimeo-%s' % key, title)
    titles.append('"%s"' % title)

  irc.msg(channel, '%s: Vimeo details: %s.' % (user, ' '.join(titles)))

def help():
  return 'Usage: vimeo <location>.'
