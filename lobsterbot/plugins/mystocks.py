# -*- coding: utf-8 -*-
import plugins.libs.cachelib as cachelib
import plugins.libs.financelib as financelib
import pickle

def run(irc, channel, user, args, useful):
  client = financelib.YahooFinance(cachelib.Cache())

  user_keys = {
    'example': {'ARM.L': (10, 954.23), 'GOOG': (9, 0.0)},
  }
  gbp_total = 0.0
  usd_total = 0.0
  stock_counter = 0
  message = ''

  if user.lower() in user_keys.keys():
    all_keys = user_keys[user.lower()]

    for stock, num_val in all_keys.iteritems():
      number, buy_value = num_val
      curr_value = float(client.CachedLookup(stock)[1])
      diff = (curr_value - buy_value) * number
      stock_counter += 1

      if stock.endswith('.L'):
        diff /= 100
        if diff < 0:
          message += 'You are down £%.2f on %s. ' % (abs(diff), stock)
        else:
          message += 'You are up £%.2f on %s. ' % (diff, stock)
        gbp_total += diff
      else:
        if diff < 0:
          message += 'You are down $%.2f on %s. ' % (abs(diff), stock)
        else:
          message += 'You are up $%.2f on %s. ' % (diff, stock)
        usd_total += diff

    irc.msg(channel, '%s: %s' % (user, message))
    if stock_counter > 1:
      message = 'That is a total of '
      if gbp_total:
        message += '£%.2f' % gbp_total
        if usd_total:
          message += ' and $%.2f' % usd_total
      else:
        if usd_total:
          message += '$%.2f' % usd_total

      irc.msg(channel, '%s: %s.' % (user, message))
  else:
    irc.msg(channel, '%s: You don\'t have any stocks registered. Speak to the admin.')
    return

def help():
  return 'mystocks.'
