import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  from datetime import date
  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  if args == '':
    tuser = user.lower()
  elif args in [ 'top 5', 'topfive', 'top five', 'top 5' ]:
    db.execute('SELECT user, CAST(numwords AS FLOAT) AS nw, CAST(charcount AS FLOAT) AS cc FROM wordstats ORDER BY nw DESC LIMIT 5')
    res = db.fetchAll()
    s = []
    for r in res:
      s.append('%s: %i, %.2f' % (r[0], r[1], r[2]/r[1]))
    irc.msg(channel, '%s: Top 5 words/av. word length - %s' % (user, str(', '.join(s))))
    return
  else:
    tuser = irc.firstArg(args).lower()

  db.executeArgs('SELECT numwords, charcount, julianday(\'now\') - julianday(first_spoke) as num_days, first_spoke FROM wordstats WHERE user = ?', (tuser, ))
  res = db.fetchOne()
  num_days = float(res[3])

  if res:
    if user.lower() == tuser:
      irc.msg(channel, '%s: You have said %i words of an average %.2f character length, %.2f words per day since %s.' % (user, res[0], float(res[1])/float(res[0]), float(res[0])/num_days, res[3]))
    else:
      irc.msg(channel, '%s: %s has said %i words of an average %.2f character length, %.2f words per day since %s.' % (user, tuser, res[0], float(res[1])/float(res[0]), float(res[0])/num_days, res[3]))
  else:
    irc.msg(channel, '%s: Have not seen %s talk.' % (user, tuser))

def help():
  return
