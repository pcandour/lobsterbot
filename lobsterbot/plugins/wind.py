import plugins.libs.sqllib as sqllib
import plugins.libs.weatherlib as weatherlib

def run(irc, channel, user, args, useful):
  if args == '' or args == '!weather':

    db = sqllib.sqllib()
    db.connect('/var/cache/remember.sql')
    db.executeArgs('SELECT location FROM userinfo WHERE user=?', (user, ))
    res = db.fetchOne()
    args = str(res[0])

  if args.startswith('!weather'):
    args = args[9:]

  if not args:
    return

  w = weatherlib.WeatherLib()
  try:
    weather = w.forecast(args, wind=True)
  except KeyError:
    irc.msg(channel, '%s: Unrecognised location, try something like UK London.' % user)
    return
  except weatherlib.WeatherError as e:
    irc.msg(channel, '%s: %s' % (user, e))
    return

  if 'possibles' in weather:
    p = weather['possibles']
    p.sort()
    irc.msg(channel, '%s: Unknown location, please choose from: %s' % (user, ', '.join(p)))
    return

  report = 'Weather for %s: Today will be %s and %dc-%dc. %s is %s and %dc-%dc, and %s will be %s and %dc-%dc.' % (args, weather[0][1], weather[0][2], weather[0][3], weather[1][0], weather[1][1], weather[1][2], weather[1][3], weather[2][0], weather[2][1], weather[2][2], weather[2][3])

  c.updateCache('wind-%s' % args.replace(' ', '_'), report)
  irc.msg(channel, report)

def help():
  return 'Usage: weather <location>.'
