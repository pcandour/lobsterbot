import plugins.libs.downtubelib as downtubelib
import plugins.libs.urlshortenerlib as urlshortener

def run(irc, channel, user, args, useful):
  if args == '':
    irc.msg(channel, '%s: You need to specify a video.' % user)
    return

  video = args.partition(' ')[0]

  my_downloader = downtubelib.MyDownloader()
  bob = downtubelib.YoutubeIE(my_downloader)
  if bob.suitable(video):
  # [{'upload_date': '20130328', 'description': u"Cassetteboy takes a look at David Cameron's school days", 'uploader_id': u'badteeth', 'format': '35 - 480x854', 'uploader': u'badteeth', 'duration': u'161', 'id': 'hwXr_-_vp9E', 'subtitles': None, 'title': u"Cassetteboy - David Cameron's School Days // Bad Teeth", 'url': u'http://r15---sn-aiglln7l.c.youtube.com/videoplayback?itag=35&ms=au&key=yt1&ip=31.222.169.104&fexp=914020%2C916611%2C929306%2C900352%2C924605%2C928201%2C901208%2C929123%2C929915%2C929906%2C925714%2C929919%2C929119%2C931202%2C932802%2C928017%2C912518%2C911416%2C906906%2C904476%2C904830%2C930807%2C919373%2C906836%2C933701%2C926403%2C912711%2C929606%2C910075&upn=EugwCmwA1ko&ipbits=8&source=youtube&newshard=yes&algorithm=throttle-factor&burst=40&sver=3&expire=1369941607&sparams=algorithm%2Cburst%2Ccp%2Cfactor%2Cid%2Cip%2Cipbits%2Citag%2Csource%2Cupn%2Cexpire&mv=m&cp=U0hVTllPT19FUUNONV9RSVNHOkNMUDZYWGlRVXdy&id=8705ebffefefa7d1&mt=1369915712&factor=1.25&signature=4FC5C29E8F7060A3AD5D286BF308C7EA91556FC2.8F1095AC1824E181A63E477A3E1CDF7DAC5CFEB5&ratebypass=yes', 'player_url': None, 'ext': 'flv', 'thumbnail': u'https://i1.ytimg.com/vi/hwXr_-_vp9E/default.jpg'}]
    values = bob.extract(video)[0]
    title = values['title']
    ext = values['ext']
    long_url = values['url']
    shorten = urlshortener.UrlShortener()
    url = shorten.shorten(long_url)

    irc.msg(channel, '%s: %s link is %s' % (user, title, url))
  else:
    irc.msg(channel, '%s: Sorry, not a recognised YouTube URL' % user)


def help():
  return 'Usage: sit <user>. Sits on somebody.'
