import plugins.libs.sqllib as sqllib

def run(irc, channel, user, args, useful):
  from time import ctime

  db = sqllib.sqllib()
  db.connect('/var/cache/remember.sql')

  if args != '':
    arg = irc.firstArg(args)
    if arg.isdigit():
      arg = int(arg) - 1
      res = db.executeArgs('SELECT id, datetime, memory FROM memory WHERE user=? AND id=?', (user, arg+1))
    else:
      res = db.executeArgs('SELECT id, datetime, memory FROM memory WHERE user=? ORDER BY datetime DESC LIMIT 5', (user, ))
  else:
    res = db.executeArgs('SELECT id, datetime, memory FROM memory WHERE user=? ORDER BY datetime DESC LIMIT 5', (user, ))

  res = db.fetchAll()

  if len(res) == 0:
    irc.msg(channel, '%s: You haven\'t asked me to remember anything.' % user)
    return

  for mem in res:
    irc.msg(channel, "%s: <%s> [%s] %s" % (user, str(mem[0]), ctime(mem[1]), str(mem[2])))

def help():
  return 'Usage: list [item number]. Retrieve your stored memories.'
