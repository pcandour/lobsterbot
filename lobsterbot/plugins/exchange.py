import re, time, os, math

def run(irc, channel, user, args, useful):
  result = convert(args, user)
  if result:
    if not result.startswith('!'):
      irc.msg(channel, result)
    else:
      result = result[1:]
      irc.msg(channel, result)
    return

  irc.msg(channel, 'Usage: Convert 10GBP to USD.')

def convert(args, user):
  currencies = {
    'AFN': ['Afghani', 'Afghanistan'],
    'ALL': ['Lek', 'Albania'],
    'DZD': ['Algerian Dinar', 'Algeria'],
    'AOA': ['Kwanza', 'Angola'],
    'ARS': ['Peso', 'Argentina'],
    'AMD': ['Dram', 'Armenia'],
    'AWG': ['Guilder', 'Aruba'],
    'AUD': ['Australian Dollar', 'Australia'],
    'AZN': ['Manat', 'Azerbaijan'],
    'BSD': ['Bahamian Dollar', 'Bahamas'],
    'BHD': ['Bahraini Dinar', 'Bahrain'],
    'BBD': ['Barbadian Dollar', 'Bajan (Barbados)'],
    'BDT': ['Taka', 'Bangladesh'],
    'BYR': ['Belarusian Ruble', 'Belarus'],
    'BZD': ['Belize Dollar', 'Belize'],
    'CFA': ['West African CFA, Franc', 'Benin'],
    'BMD': ['Bermudian Dollar', 'Bermuda'],
    'BTN': ['Bhutanese Ngultrum', 'Bhutan'],
    'INR': ['India Rupee', 'Bhutan'],
    'BOB': ['Boliviano', 'Bolivia'],
    'BAM': ['Convertible Marka', 'Bosnia and Herzegovina'],
    'BWP': ['Pula', 'Botswana'],
    'BRL': ['Real', 'Brazil'],
    'BND': ['Brunei Dollar', 'Brunei'],
    'SGD': ['Singapore Dollar', 'Brunei'],
    'BGN': ['Lev', 'Bulgaria'],
    'CFA': ['West African CFA, Franc', 'Burkina Faso'],
    'MMK': ['Kyat', 'Burma (Myanmar)'],
    'BIF': ['Burundi Franc', 'Burundi'],
    'KHR': ['Riel', 'Cambodia'],
    'CFA': ['West African CFA, Franc', 'Cameroon'],
    'CAD': ['Canadian Dollar', 'Canada'],
    'CVE': ['Cape Verdean Escudo', 'Cape Verde'],
    'KYD': ['Cayman Islands Dollar', 'Cayman Islands'],
    'CFA': ['West African CFA, Franc', 'Central African Republic'],
    'XAF': ['BEAC Franc', 'Communaut&eacute; Financi&egrave;re Africaine'],
    'XOF': ['BCEAO Franc', 'Communaut&eacute; Financi&egrave;re Africaine'],
    'XAF': ['CFA Franc BEAC', 'Chad'],
    'CLP': ['Peso', 'Chile'],
    'CNY': ['Yuan Renminbi', 'China'],
    'CLP': ['Peso', 'Colombia'],
    'KMF': ['Comorian Franc', 'Comoros'],
    'XAF': ['Central African CFA franc', 'Congo/Brazzaville'],
    'CDF': ['Congonese Franc', 'Kinshasa'],
    'NZD': ['New Zealand Dollar, Cook Islands Dollar', 'Cook Islands'],
    'CRC': ['Costa Rican Colon', 'Costa Rica'],
    'XOF': ['West African CFA, Franc', 'C&ocirc;te d&#39;Ivoire'],
    'HRK': ['Kuna', 'Croatia'],
    'CUC': ['Cuban Convertible Peso', 'Cuba'],
    'CUP': ['Cuban Peso', 'Cuba'],
    'CZK': ['Czech Koruna', 'Czech Republic'],
    'DKK': ['Danish Krone', 'Denmark'],
    'DJF': ['Djibouti Franc', 'Djibouti'],
    'DOP': ['Dominican Peso', 'Dominican Republic'],
    'XCD': ['East Caribbean Dollar', 'East Caribbean'],
    'EGP': ['Egyptian Pound', 'Egypt'],
    'SVC': ['Salvadoran Colon', 'El Salvador'],
    'XAF': ['Central African CFA franc', 'Equatorial Guinea'],
    'ERN': ['Eritrean Nakfa', 'Eritrea'],
    'EEK': ['Estonian Kroon', 'Estonia'],
    'ETB': ['Ethiopian Birr', 'Ethiopia'],
    'EUR': ['Euro', 'European Union'],
    'FKP': ['Falkland Islands Pound', 'Falkland Islands'],
    'DKK': ['Faroese Krona', 'Faroe Islands'],
    'FJD': ['Fijian Dollar', 'Fiji'],
    'XPF': ['CFP Franc', 'France'],
    'XAF': ['Central African CFA franc', 'Gabon'],
    'GMD': ['Gambian Dalasi', 'Gambia'],
    'ILS': ['Israel New Shekel', 'Gaza Strip'],
    'EGP': ['Egyptian Pound', 'Gaza Strip'],
    'GEL': ['Georgian Lari', 'Georgia'],
    'GHS': ['Cedi', 'Ghana'],
    'GIP': ['Gibraltar Pound', 'Gibraltar'],
    'DKK': ['Denmark Krone', 'Greenland'],
    'GTQ': ['Guatemalan Quetzal', 'Guatemala'],
    'GGP': ['Pound Sterling', 'Guernsey'],
    'GNF': ['Guinean Franc', 'Guinea'],
    'XOF': ['West African CFA, Franc', 'Guinea-Bissau'],
    'GYD': ['Guyanese Dollar', 'Guyana'],
    'HTG': ['Haitian Gourde', 'Haiti'],
    'HNL': ['Honduran Lempira', 'Honduras'],
    'HKD': ['Hong Kong Dollar', 'Hong Kong'],
    'HUF': ['Hungarian Forint', 'Hungary'],
    'ISK': ['Icelandic Krona', 'Iceland'],
    'INR': ['Indian Rupee', 'India'],
    'IDR': ['Indonesian Rupiah', 'Indonesia'],
    'IRR': ['Iranian Rial', 'Iran'],
    'IQD': ['Iraqi Dinar', 'Iraq'],
    'GBP': ['Pound Sterling', 'Ireland'],
    'FKP': ['Falkland Islands Pound', 'Islas Malvinas (Falkland Islands)'],
    'GBP': ['Pound Sterling', 'Isle of Man'],
    'ILS': ['Israeli New Shekel', 'Israel'],
    'XOF': ['West African CFA, Franc', 'Ivory Coast (C&ocirc;te d&#39;Ivoire)'],
    'JMD': ['Jamaican Dollar', 'Jamaica'],
    'JPY': ['Yen', 'Japan'],
    'GBP': ['Pound Sterling', 'Jersey'],
    'JOD': ['Jordanian Dinar', 'Jordan'],
    'KZT': ['Kazakhstani Tenge', 'Kazakhstan'],
    'KES': ['Kenyan Shilling', 'Kenya'],
    'KID': ['Kiribati Dollar', 'Kiribati'],
    'KWD': ['Kuwaiti Dinar', 'Kuwait'],
    'KGS': ['Kyrgyzstani Som', 'Kyrgyzstan'],
    'LAK': ['Lao Kip', 'Laos'],
    'LVL': ['Latvian Lats', 'Latvia'],
    'LBP': ['Lebanese Pound', 'Lebanon'],
    'LSL': ['Lesotho Loti', 'Lesotho'],
    'LRD': ['Liberian Dollar', 'Liberia'],
    'LYD': ['Libyan Dinar', 'Libya'],
    'CHF': ['Swiss Franc', 'Liechtenstein'],
    'LTL': ['Lithuanian Litas', 'Lithuania'],
    'MOP': ['Macanese Pataca', 'Macau'],
    'MKD': ['Macedonian Denar', 'Republic of Macedonia'],
    'MGA': ['Malagasy Ariary', 'Madagascar'],
    'MWK': ['Malawian Kwacha', 'Malawi'],
    'MYR': ['Malaysian Ringgit', 'Malaysia'],
    'MVR': ['Maldivian Rufiyaa', 'Maldives'],
    'XOF': ['West African CFA, Franc', 'Mali'],
    'FKP': ['Falkland Islands Pound', 'Malvinas (Falkland Islands)'],
    'MRO': ['Mauritanian Ouguiya', 'Mauritania'],
    'MUR': ['Mauritian Rupee', 'Mauritius'],
    'MXN': ['Mexican Peso', 'Mexico'],
    'MDL': ['Moldovan Leu', 'Moldova'],
    'MNT': ['Mongolian Tughrik (t&ouml;gr&ouml;g)', 'Mongolia'],
    'MAD': ['Moroccan Dirham', 'Morocco'],
    'MZN': ['Mozambican Metical', 'Mozambique'],
    'MMK': ['Myanma Kyat', 'Myanmar (Burma)'],
    'NAD': ['Namibian Dollar', 'Namibia'],
    'HTG': ['Haitian Gourde', 'Navassa'],
    'NPR': ['Nepalese Rupee', 'Nepal'],
    'ANG': ['Netherlands Antilles Guilder', 'Netherlands Antilles'],
    'NZD': ['New Zealand Dollar', 'New Zealand'],
    'NIO': ['Nicaraguan Cordoba', 'Nicaragua'],
    'XOF': ['West African CFA, Franc', 'Niger'],
    'NGN': ['Nigerian Naira', 'Nigeria'],
    'NZD': ['New Zealand Dollar', 'Niue'],
    'KPW': ['North Korean Won', 'North Korea'],
    'NOK': ['Norwegian Krone', 'Norway'],
    'OMR': ['Omani Rial', 'Oman'],
    'PKR': ['Pakistani Rupee', 'Pakistan'],
    'PAB': ['Panamanian Balboa', 'Panama'],
    'PGK': ['Papua New Guinean Kina', 'Papua New Guinea'],
    'PYG': ['Paraguayan Guarani', 'Paraguay'],
    'PEN': ['Peruvian Nuevo Sol', 'Peru'],
    'PHP': ['Philippine Peso', 'Philippines'],
    'NZD': ['New Zealand Dollar', 'Pitcairn Islands'],
    'PLN': ['Polish Zloty', 'Poland'],
    'QAR': ['Qatari Riyal', 'Qatar'],
    'RON': ['Romanian Leu', 'Romania'],
    'RUB': ['Russian Ruble', 'Russia'],
    'RWF': ['Rwandan Franc', 'Rwanda'],
    'SHP': ['Saint Helena Pound', 'Saint Helena'],
    'WST': ['Samoan Tala', 'Samoa'],
    'STD': ['S&atilde;o Tom&eacute; and Pr&iacute;ncipe Dobra', 'S&atilde;o Tome and Principe'],
    'SAR': ['Saudi Riyal', 'Saudi Arabia'],
    'GBP': ['Pound Sterling', 'Scotland (United Kingdom)'],
    'SPL': ['Seborga Luigino', 'Seborga'],
    'XOF': ['West African CFA, Franc', 'Senegal'],
    'RSD': ['Serbian Dinar', 'Serbia'],
    'SCR': ['Seychellois Rupee', 'Seychelles'],
    'SLL': ['Sierra Leonean Leone', 'Sierra Leone'],
    'SGD': ['Singapore Dollar', 'Singapore'],
    'SBD': ['Solomon Islands Dollar', 'Solomon Islands'],
    'SOS': ['Somali Shilling', 'Somalia'],
    'ZAR': ['South African Rand', 'South Africa'],
    'GBP': ['Pound Sterling', 'South Georgia'],
    'KRW': ['South Korean Won', 'South Korea'],
    'GBP': ['Pound Sterling', 'South Sandwich Islands'],
    'LKR': ['Sri Lankan Rupee', 'Sri Lanka'],
    'SDG': ['Sudanese Pound', 'Sudan'],
    'SRD': ['Surinamese Dollar', 'Suriname'],
    'NOK': ['Norwegian Krone', 'Svalbard and Jan Mayen'],
    'SZL': ['Swazi Lilangeni', 'Swaziland'],
    'ZAR': ['South African Rand', 'Swaziland'],
    'SEK': ['Swedish Krona', 'Sweden'],
    'CHF': ['Swiss Franc', 'Switzerland'],
    'SYP': ['Syrian Pound', 'Syria'],
    'TWD': ['New Taiwan Dollar', 'Taiwan'],
    'TJS': ['Tajikistani Somoni', 'Tajikistan'],
    'RUB': ['Russian Ruble', 'Tajikistan'],
    'TZS': ['Tanzanian Shilling', 'Tanzania'],
    'THB': ['Thai Baht', 'Thailand'],
    'XOF': ['West African CFA, Franc', 'Togo'],
    'NZD': ['New Zealand Dollar', 'Tokelau'],
    'TOP': ['Tongan Pa&#39;anga', 'Tonga'],
    'MDL': ['Transnistrian Ruble', 'Transnistria'],
    'TTD': ['Trinidad and Tobago Dollar', 'Trinidad and Tobago'],
    'TND': ['Tunisian Dinar', 'Tunisia'],
    'TRY': ['Turkish Lira', 'Turkey'],
    'TMM': ['Turkmenistan Manat', 'Turkmenistan'],
    'TVD': ['Tuvaluan Dollar', 'Tuvalu'],
    'UGX': ['Ugandan Shilling', 'Uganda'],
    'UAH': ['Ukrainian Hryvna', 'Ukraine'],
    'AED': ['United Arab Emirates Dirham', 'United Arab Emirates'],
    'GBP': ['Pound Sterling', 'United Kingdom'],
    'USD': ['United States Dollar', 'United States of America'],
    'UYU': ['Uruguayan Peso', 'Uruguay'],
    'UZS': ['Uzbekistani Som', 'Uzbekistan'],
    'VUV': ['Vanuatu Vatu', 'Vanuatu'],
    'VEF': ['Venezuelan Bolivar', 'Venezuela'],
    'VND': ['Vietnamese Dong', 'Vietnam'],
    'ILS': ['Israeli New Shekel', 'West Bank'],
    'JOD': ['Jordanian Dinar', 'West Bank'],
    'MAD': ['Moroccan Dirham', 'Western Sahara'],
    'MAD': ['Sahrawi Peseta', 'Western Sahara'],
    'WST': ['Samoan Tala', 'Samoa'],
    'YER': ['Yemeni Rial', 'Yemen'],
    'ZMK': ['Zambian Kwacha', 'Zambia'],
  }

  import plugins.libs.keyvaluelib as keyvaluelib
  kv = keyvaluelib.Keyvalue()

  currnames = dict([(x[1][0], x[0]) for x in currencies.items()])
  if args == '':
    return

  if args.startswith('!convert'):
    args = args[8:]

  if args.startswith('!exchange'):
    args = args[9:]

  args = args.split(' ')

  if len(args) < 3:
    return None

  if args[0] == 'alias':
    symbol = args.pop().upper()
    alias = args.pop().upper()

    if not alias.alpha():
      return 'Not aliasing to a non-alpha string.'
    if len(symbol) != 3:
      return '!Invalid alias or symbol.'
    if symbol not in currencies:
      return '!I don\'t know that currency to alias it.'
    if alias in currencies:
      return '!That is a real currency.'
    kv.updateKeyvalue('curralias', alias, symbol)
    return '!I will now recognise %s as %s.' % (alias, symbol)

  tc = args.pop()
  to = args.pop()
  if to != 'to' and to != 'in' and to != 'as':
    tc = tc + to
  fc = args.pop()

  pound = chr(194) + chr(163)
  yuan = chr(194) + chr(165)
  euro = chr(226) + chr(130) + chr(172)
  tc = tc.replace(pound, 'GBP')
  tc = tc.replace(euro, 'EUR')
  tc = tc.replace(yuan, 'CNY')
  tc = tc.replace('r$', 'BRL')
  tc = tc.replace('$', 'USD')
  if str(fc).startswith(pound):
    fc = fc[2:] + 'GBP'
  if str(fc).startswith(yuan):
    fc = fc[2:] + 'CNY'
  if str(fc).startswith(euro):
    fc = fc[3:] + 'EUR'
  if str(fc).startswith('r$'):
    fc = fc[2:] + 'BRL'
  if str(fc).startswith('$'):
    fc = fc[1:] + 'USD'
  if len(fc) > 3:
    results = re.match(r'([0-9]+)([A-Za-z]+)', fc)
    if results:
      fa, fc = results.groups()
    else:
      return "Could not understand %s." % fc
  else:
    fa = args.pop()

  if not fa.replace('.', '').isdigit() or not fc.isalpha() or not tc.isalpha():
    return None

  currtime = int(math.floor(time.time()))
  last_update = int(kv.getKeyvalue('updatetime', 'curr'))
  updated = '(last updated %s seconds ago.)' % (currtime - last_update)
  if time.time() > last_update + 60 * 60:
    os.system('/var/cache/updateCurrency.py')
    kv.updateKeyvalue('updatetime', 'curr', currtime)
    updated = '(Cache updated, was last updated %s seconds ago.)' % (currtime - last_update)

  fc = fc.upper()
  tc = tc.upper()

  fa = float(fa)

  alias = kv.getKeyvalue('curralias', fc)
  fc = alias or fc
  alias = kv.getKeyvalue('curralias', tc)
  tc = alias or tc

  fromrate = float(kv.getKeyvalue('curr', fc))
  torate = float(kv.getKeyvalue('curr', tc))

  if fromrate and torate:
    converted = 1 / fromrate * torate * fa
    return '%s: %.02f %ss is %f %s. %s' % (user, fa, currencies[fc][0], converted, currencies[tc][0], updated)

  return None

def help():
  return 'Usage: Convert 10GBP to USD, or convert alias <alias> <symbol>.'
