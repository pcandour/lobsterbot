import os
import sys

if "PLUGIN_PATH" not in os.environ or "PLUGIN_REPO" not in os.environ:
    print("You must set both PLUGIN_PATH and PLUGIN_REPO environment variables.")
    sys.exit(1)

plugin_path = os.environ["PLUGIN_PATH"]
repo = os.environ["PLUGIN_REPO"]

if not os.path.isdir(plugin_path):
    os.mkdir(plugin_path)

if not os.path.isdir("%s/.git" % plugin_path):
    os.system("git clone \"%s\" \"%s\"" % (repo, plugin_path))

os.chdir(plugin_path)
os.system("git pull")

